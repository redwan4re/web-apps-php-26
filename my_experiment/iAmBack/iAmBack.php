<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 8/28/2016
 * Time: 2:15 AM
 */

echo "hi, I am Back again";

$arr= array('FirstName'=> 'muhammad','lastName'=>'redwan');
echo "<pre>";
print_r($arr);

$test='';
if (empty($test)){
    echo "this is empty";
}
else{
    echo $test;
}

echo "<pre>";
echo date('M d, Y l t  H:i:s ') . "<br>";
echo date('d\t\h\ M, Y ');


echo "<br>";

//print_r(getdate());


// Prints the whole date
echo date('r') . "<br>";

// Prints the day
echo date("l") . "<br>";

// Prints the day, date, month, year, time, AM or PM
echo date("l jS \of F Y h:i:s A") . "<br>";

// Prints December 16, 1971 was on a Thursday
echo "December 16, 1971 was on a " . date("l", mktime(0, 0, 0, 12, 16, 1971)) . "<br>";

// Use a constant in the format parameter
echo date(DATE_RFC822) . "<br>";

// prints something like: 1975-10-03T00:00:00+00:00
echo date(DATE_ATOM, mktime(0, 0, 0, 10, 3, 1975));


// set the default timezone to use. Available since PHP 5.1
date_default_timezone_set('UTC');

/*
 * PHP.NET manual
 */
echo "<br>";
// Prints something like: Monday
echo date("l");

// Prints something like: Monday 8th of August 2005 03:12:46 PM
echo date('l jS \of F Y h:i:s A') . "<br>";

// Prints: July 1, 2000 is on a Saturday
echo "July 1, 2000 is on a " . date("l", mktime(0, 0, 0, 7, 1, 2000)) . "<br>";

/* use the constants in the format parameter */
// prints something like: Wed, 25 Sep 2013 15:28:57 -0700
echo date(DATE_RFC2822) . "<br>";

// prints something like: 2000-07-01T00:00:00+00:00
echo date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000)) . "<br>";


// prints something like: Wednesday the 15th
echo date('l \t\h\e jS');