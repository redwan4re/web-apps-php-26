<html>
<head>
    <title>This is a simple calculator</title>

    <style>
        body {

            background-color: lightblue;
            padding: 0px;

        }

        .main {
            width: 960px;
            margin: 10px auto;
        }


    </style>

</head>


<body>
<div class="main">

    <h1> A simple calculator</h1>

    <form method="POST" action="calc3.php">
        <label><h2>Enter two numbers to add/subtract/multiply/divide</h2></label>
        <br>
        
        <input type="number" name="num1" placeholder="Enter Number 1"> 
        <select name="calculate">
            <option>Sum</option>
            <option>Subtract</option>
            <option>Multiply</option>
            <option>Divide</option>
        </select>
        <!--  <select>
            <option>+</option>
            <option>-</option>
            <option>*</option>
            <option>/</option>
        </select> -->
        <input type="number" name="num2" placeholder="Enter Number 2"> 
        <button type="submit">Calculate</button>
        <br/>
        <h5><button type="reset">Reset</button></h5>
        <br>

    </form>


</div>
</body>
</html>