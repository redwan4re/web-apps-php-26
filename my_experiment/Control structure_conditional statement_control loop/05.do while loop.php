<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 11:59 AM
 */


$x = 1;
do {
    echo "klyopa <br>";
    $x++;
} while ($x <= 5);
/*Notice that in a do while loop the condition is tested
 AFTER executing the statements within the loop.
This means that the do while loop would execute its
statements at least once, even if the condition is false the first time.
*/

$x = 6;
do {
    echo $x."<br>";
$x++;
} while ($x <= 5);
// this will execute the code first, then it will check the condition.
//so output will be 6.