<?php

/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 6/28/2016
 * Time: 3:02 PM
 */
class Person
{
//    public $name="muhammad redwan";
    public $name;
    public $age;

    public function personName()
    {
        echo "Person NAME is: $this->name <br/>";
//echo "Person NAME is:" . $this->name . "<br/>";
    }

    public function personAge()
//    public function personAge($value)
    {
//       echo "Person AGE is: " . $this->age=$value ."<br/>";
        echo "Person AGE is: $this->age <br/>";
    }
}

$obj = new Person();

$obj->name = "Muhammad Redwan";
$obj->age = "117";

//echo $obj->name;

$obj->personName();
$obj->personAge();
//$obj->personAge("29");