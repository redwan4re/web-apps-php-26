<?php

/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 6/28/2016
 * Time: 4:50 PM
 */
class Calculator
{
    public $num1 = "";
    public $num2 = "";

    public function add($a = '', $b = '')
    {
        $this->num1 = $a;
        $this->num2 = $b;
        echo "number 1 =" . $this->num1. "<br>";
        echo "number 2 =" . $this->num2. "<br><hr>" ;
        echo "Summation is = " . ($this->num1 + $this->num2) . "<br/>";
    }

    /*we just reduced the code by assigning object operator "$this->"
    eta na hole sub(), mul() and div() er jonno o ekivabe varibale assign korte
    hoto--- sub($a='', $b='')
    */
    public function sub()
    {
        echo "subtraction is = " . ($this->num1 - $this->num2) . "<br/>";
    }

    public function mul()
    {
        echo "multiplication is = " . ($this->num1 * $this->num2) . "<br/>";
    }

    public function div()
    {
        echo "division is = " . ($this->num1 / $this->num2) . "<br/>";
    }

}