<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 1:29 PM
 */

/*Using for loop Create a script that displays 1-2-3-4-5-6-7-8-9-10 on one line.
There will be no hyphen(-) at starting and ending position*/


for ($x = 1; $x <= 10; $x++) {
    if ($x <= 9)
        echo $x . '-';
    else {
        echo $x;
    }

}

echo "<br>";

for ($x = 1; $x <= 10; $x++) {
    if ($x <= 9)
        echo $x . '-';
    else {
        echo $x;
    }

}