<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 2:28 PM
 */

/*Create a script to construct the following pattern, using nested for loop.

*
* *
* * *
* * * *
* * * * *   */
echo "<hr> Increment <br>";

for ($x = 1; $x <= 5; $x++) {
    if ($x = 1) {
        echo "* <br>";
    }
    if ($x = 2) {
        echo "**<br>";
    }
    if ($x = 3) {
        echo "***<br>";
    }
    if ($x = 4) {
        echo "****<br>";
    }
    if ($x = 5) {
        echo "*****<br>";
    }
}
echo "<hr> decrement <br>";

for ($x = 5; $x >= 1; $x--) {
    if ($x = 5) {
        echo "***** <br>";
    }
    if ($x = 4) {
        echo "****<br>";
    }
    if ($x = 3) {
        echo "***<br>";
    }
    if ($x = 2) {
        echo "**<br>";
    }
    if ($x = 1) {
        echo "*<br>";
    }
}
echo "<hr>";

// smart code

echo "With smart code <br>";

for ($x = 1; $x <= 5; $x++) {
    for ($y = 1; $y <= $x; $y++) {
        echo "*";
//        for adding a space between two *
        if ($y < $x) {
            echo " ";
        }
    }
    echo "<br>";
}


for ($x = 4; $x >= 1; $x--) {
    for ($y = 1; $y <= $x; $y++) {
        echo "*";
        // for adding a space between two *
        if ($y < $x) {
            echo " ";
        }
    }
    echo "<br>";
}