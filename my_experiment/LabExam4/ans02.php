<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 1:05 PM
 */
/*2. $color = array('white', 'green', 'red');

Write a PHP script which will display the colors in the following way :
Output :
white, green, red,

.green (bullet)
.red
.white*/

$color = array('white', 'green','red');

echo $color[0]. ', ' .$color[1]. ', ' .$color[2];

echo "<ul>";
echo "<li>".$color[1]."</li> ";
echo "<li>".$color[2]."</li> ";
echo "<li>".$color[0]."</li> ";
