<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 1:59 PM
 */
/*http://www.w3resource.com/php-exercises/php-for-loop-exercises.php
 * 2. Create a script using a for loop to add all the integers between 0 and 30
and display the total.*/

$TotalSum = 0;

for ($x =1; $x<=30; $x++){

 $TotalSum +=$x;

}
echo "Total sum of 1 to 30 is:  $TotalSum";