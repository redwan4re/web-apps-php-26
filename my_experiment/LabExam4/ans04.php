<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/1/2016
 * Time: 1:21 PM
 */http://www.w3resource.com/php-exercises/php-array-exercises.php (q.19)
/*Write a PHP script to print "second" and Red from the following array.
Sample Data :
$color = array ( "color" => array ( "a" => "Red", "b" => "Green", "c" => "White"),
    "numbers" => array ( 1, 2, 3, 4, 5, 6 ),
    "holes" => array ( "First", 5 => "Second", "Third"));*/

$color = array ( "color" => array ( "a" => "Red", "b" => "Green", "c" => "White"),
    "numbers" => array ( 1, 2, 3, 4, 5, 6 ),
    "hotels" => array ( "First", 5 => "Second", "Third"));

//echo "<pre>";
//print_r($color);

echo $color['color']['a']. "<br>";
echo $color['hotels']['5'];