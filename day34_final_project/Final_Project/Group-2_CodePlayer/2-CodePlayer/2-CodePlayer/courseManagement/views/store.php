<?php
include_once '../vendor/autoload.php';
use Prog\Course;

$obj = new Course();
$obj->prepare($_POST)->store();
