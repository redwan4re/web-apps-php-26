<?php
include_once '../vendor/autoload.php';
use Prog\Course;

$obj = new Course();

$allTrainers = $obj->AllTrainers();

$allLabs = $obj->AllLabs();

$allData = $obj->allData();
//echo "<pre>";
//print_r($allTrainers);
//die();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BITM Lab Management</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="assets/js/core/app.js"></script>
    <script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Lab Management Software</a>
        <!--        <a class="navbar-brand" href="../index.php"><img src="assets/images/logo_light.png" alt=""></a>-->


        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/placeholder.jpg" alt="">
                    <span>CodePlayer</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">BITM</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Karwan Bazar
                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Code Player</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li>
                                <a href="#"><i class="icon-stack2"></i> <span>User Management</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-copy"></i> <span>Trainer Management</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-droplet2"></i> <span>Course Information</span></a>

                            </li>
                            <li>
                                <a href="#"><i class="icon-list-unordered"></i> <span>Lab Information</span></a>
                            </li>
                            <li><a href="index.php"><i class="icon-list-unordered"></i> <span>Software Management</span></a></li>
                            <li class="active"><a href="index.php"><i class="icon-list-unordered"></i> <span>Course Management</span></a></li>


                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- Content area -->
        <div class="content">
            <div class="row">
                <form class="form-horizontal" action="store.php" method="post">
                    <fieldset class="content">
                        <legend class="text-bold">Basic inputs</legend>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Course Title:</label>
                                <div class="col-md-9" id="output1">
                                    <select id="" onchange="showUser(this.value)" class="form-control" name="course_id">
                                        <option value="">select title</option>
                                        <?php
                                        if (!empty($allData)) {
                                            foreach ($allData as $item) {
                                                ?>
                                                <option value="<?php echo $item['id']; ?>"><?php
                                                    if (!empty($item['title'])) {
                                                        echo $item['title'];
                                                    }
                                                    ?></option>
                                            <?php }
                                        }
                                        ?>
                                    </select>
                                    <script>
                                        function showUser(str) {
                                            if (str == "") {
                                                document.getElementById("txtHint").innerHTML = "";
                                                return;
                                            } else {
                                                if (window.XMLHttpRequest) {
                                                    // code for IE7+, Firefox, Chrome, Opera, Safari
                                                    xmlhttp = new XMLHttpRequest();
                                                } else {
                                                    // code for IE6, IE5
                                                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                                }
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                                        document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                                                    }
                                                };
                                                xmlhttp.open("GET","livedata.php?q="+str,true);
                                                xmlhttp.send();
                                            }
                                        }
                                    </script>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Batch No:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" placeholder="Enter Batch Number" name="batch_no">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Lead Trainer:</label>
                                <div class="col-md-9" id="output1">
                                    <select id="" onchange="showUser(this.value)" class="form-control" name="course_id">
                                        <option selected="selected">Select Lead Trainer</option>
                                        <?php
                                        foreach ($allTrainers as $oneTrainer) {
                                            if ($oneTrainer['trainer_status'] == "lead_trainer") {
                                                ?>
                                                <option value="<?php echo $oneTrainer['full_name'] ?>"><?php echo $oneTrainer['full_name'] ?> </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <script>
                                        function showUser(str) {
                                            if (str == "") {
                                                document.getElementById("txtHint").innerHTML = "";
                                                return;
                                            } else {
                                                if (window.XMLHttpRequest) {
                                                    // code for IE7+, Firefox, Chrome, Opera, Safari
                                                    xmlhttp = new XMLHttpRequest();
                                                } else {
                                                    // code for IE6, IE5
                                                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                                }
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                                        document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                                                    }
                                                };
                                                xmlhttp.open("GET","livedata.php?q="+str,true);
                                                xmlhttp.send();
                                            }
                                        }
                                    </script>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Assistant Trainer:</label>
                                <div class="col-md-9" id="output1">
                                    <select id="" onchange="showUser(this.value)" class="form-control" name="course_id">
                                        <option selected="selected">Select Assistant Trainer</option>
                                        <?php
                                        foreach ($allTrainers as $oneTrainer) {
                                            if ($oneTrainer['trainer_status'] == "assist_trainer") {
                                                ?>
                                                <option class="conditional <?php echo $oneTrainer['courses_id']; ?>" value="<?php echo $oneTrainer['full_name'] ?>"><?php echo $oneTrainer['full_name'] ?> </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <script>
                                        function showUser(str) {
                                            if (str == "") {
                                                document.getElementById("txtHint").innerHTML = "";
                                                return;
                                            } else {
                                                if (window.XMLHttpRequest) {
                                                    // code for IE7+, Firefox, Chrome, Opera, Safari
                                                    xmlhttp = new XMLHttpRequest();
                                                } else {
                                                    // code for IE6, IE5
                                                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                                }
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                                        document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                                                    }
                                                };
                                                xmlhttp.open("GET","livedata.php?q="+str,true);
                                                xmlhttp.send();
                                            }
                                        }
                                    </script>
                                </div>

                            </div>
                        </div>

                        <script>
                            function showUser1(str) {
                                if (str == "") {
                                    document.getElementById("txtHint1").innerHTML = "";
                                    return;
                                } else {
                                    if (window.XMLHttpRequest) {
                                        // code for IE7+, Firefox, Chrome, Opera, Safari
                                        xmlhttp = new XMLHttpRequest();
                                    } else {
                                        // code for IE6, IE5
                                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                                    }
                                    xmlhttp.onreadystatechange = function() {
                                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                            document.getElementById("txtHint1").innerHTML = xmlhttp.responseText;
                                        }
                                    };
                                    xmlhttp.open("GET","getassttrainer.php?q="+str,true);
                                    xmlhttp.send();
                                }
                            }
                        </script>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Lab Assistant:</label>
                                <div class="col-md-9">
                                    <select id="os" onchange="ChangeCarList()" class="form-control" name="lab_asst">
                                        <option selected="selected">Select Lab Assistant</option>
                                        <?php
                                        foreach ($allTrainers as $oneTrainer) {
                                            if ($oneTrainer['trainer_status'] == "lab_assist") {
                                                ?>
                                                <option value="<?php echo $oneTrainer['full_name'] ?>"><?php echo $oneTrainer['full_name'] ?> </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Lab No:</label>
                                <div class="col-md-9">
                                    <select id="os" onchange="" class="form-control" name="lab_id">
                                        <?php
                                        foreach ($allLabs as $oneLabs) {
                                            ?>
                                            <option value="<?php echo $oneLabs['lab_no'] ?>">Lab <?php echo $oneLabs['lab_no'] ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Start Date:</label>
                                <div class="col-md-9">
                                    <input type="date"  id="datepicker" class="form-control"  name="start_date" value="" placeholder="Select Date">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Ending Date:</label>
                                <div class="col-md-9">
                                    <input type="date"  id="datepickerend" class="form-control"  name="ending_date" value="" placeholder="Select Date">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Start Time:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="time" name="start_time" value="">

                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Ending Time:</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="ending_time" value="">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Day:</label>
                                <div class="col-md-9">
                                    <select id="os" onchange="" class="form-control" name="day">
                                        <option value="Sat,Mon,Wed">Saturday,Monday,Wednesday</option>
                                        <option value="Sun,Tue,Thu">Sunday,Tuesday,Thursday</option>
                                        <option value="Friday">Friday</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary col-md-2 pull-right">Submit <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </fieldset>


                </form>
            </div>
        </div>







            <!-- Footer -->
            <div class="footer text-muted">
                &copy; 2016.  <a href="#" target="_blank">WebApps Develop-PHP(Batch 26)</a> by Group-2: <a href="#">CodePlayer</a> | Mentor:<a href="#">Md. AbuTaleb</a>
            </div>
            <!-- /footer -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
