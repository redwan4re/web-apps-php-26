<?php

namespace Prog;
use PDO;

class Course
{
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = '';
    public $id = '';
    public $course_id = '';
    public $batch_no = '';
    public $lead_trainer = '';
    public $asst_trainer = '';
    public $lab_asst = '';
    public $lab_id = '';
    public $start_date = '';
    public $ending_date = '';
    public $start_time = '';
    public $ending_time = '';
    public $day = '';
    public $is_running = '';
    public $assing_by = '';
    public $created = '';
    public $modified = '';
    public $delete = '';
    public $data = '';
    public $livedata = '';
    public $getlab = '';
    public $gettrain = '';

    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        try{
            $this->conn = new PDO("mysql:host=localhost;dbname=lab", $this->dbuser, $this->dbpass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function prepare($data = "")
    {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['course_id'])) {
            $this->course_id = $data['course_id'];
        }
        if (!empty($data['batch_no'])) {
            $this->batch_no = $data['batch_no'];
        }
        if (!empty($data['lead_trainer'])) {
            $this->lead_trainer = $data['lead_trainer'];
        }
        if (!empty($data['asst_trainer'])) {
            $this->asst_trainer = $data['asst_trainer'];
        }
        if (!empty($data['lab_asst'])) {
            $this->lab_asst = $data['lab_asst'];
        }
        if (!empty($data['lab_id'])) {
            $this->lab_id = $data['lab_id'];
        }
        if (!empty($data['start_date'])) {
            $this->start_date = $data['start_date'];
        }
        if (!empty($data['ending_date'])) {
            $this->start_date = $data['ending_date'];
        }
        if (!empty($data['start_time'])) {
            $this->start_time = $data['start_time'];
        }
        if (!empty($data['ending_time'])) {
            $this->ending_time = $data['ending_time'];
        }
        if (!empty($data['day'])) {
            $this->day = $data['day'];
        }

        return $this;

    }
    // Show AllCourse
    public function allData()
    {
        try {
            $query = "SELECT DISTINCT `title`, `id` FROM courses";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    // AllTrainers
    public function AllTrainers()
    {
        $query = "SELECT DISTINCT `courses_id`,`trainer_status`, `team`,`full_name` FROM `trainers`";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->trainer[] = $row;
        }
        return $this->trainer;

    }
    // AllLabs
    public function AllLabs()
    {
        $query = "SELECT DISTINCT `lab_no` FROM labinfo";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->lab[] = $row;
        }
        return $this->lab;

    }
    //validation
//    public function validation()
//    {
//        if (!empty($this->course_name)) {
//            $_SESSION['course_name_V'] = $this->course_name;
//        } else {
//            $_SESSION['course_name_M'] = "Please enter course name *";
//            $this->errors = true;
//        }
//
//        if (!empty($this->course_duration)) {
//            $_SESSION['course_duration_V'] = $this->course_duration;
//        } else {
//            $_SESSION['course_duration_M'] = "Please select course duration *";
//            $this->errors = true;
//        }
//
//        if (!empty($this->course_type)) {
//            if ($this->course_type == 1) {
//                if (!empty($this->course_fee)) {
//                    $_SESSION['course_fee_V'] = $this->course_fee;
//                } else {
//                    $_SESSION['course_fee_M'] = "Please enter course fee *";
//                    $this->errors = true;
//                }
//            }
//            $_SESSION['course_type_V'] = $this->course_type;
//        } else {
//            $_SESSION['course_type_M'] = "Please select course type *";
//            $this->errors = true;
//        }
//    }

    //store
    public function store(){
        if (!empty($this->batch_no) && !empty($this->course_id)){
            try {
                $query = "INSERT INTO course_trainer_lab_mapping (id, course_id, batch_no, lead_trainer, asst_trainer, lab_asst, lab_id, start_date, ending_date, start_time, ending_time, day, is_running, assigned_by, created, updated, deleted) 
                          VALUES (:id, :cid, :bt, :lt, :ast, :laba, :labid, :sd, :ed, :st, :et, :d, :is_r, :asb, :ct, :upd, :del)";
                $stmt = $this->conn->prepare($query); //this prepare method is form PDO class
                $stmt->execute(array(
                    ":id"=>$this->id,
                    ":cid"=>$this->course_id,
                    ":bt"=>$this->batch_no,
                    ":lt"=>$this->lead_trainer,
                    ":ast"=>$this->asst_trainer,
                    ":laba"=>$this->lab_asst,
                    ":labid"=>$this->lab_id,
                    ":sd"=>$this->start_date,
                    ":ed"=>$this->ending_date,
                    ":st"=>$this->start_time,
                    ":et"=>$this->ending_time,
                    ":d"=>$this->day,
                    ":is_r"=>$this->is_running,
                    ":asb"=>$this->assing_by,
                    ":ct"=>date("Y-m-d h:i:s"),
                    ":upd"=> '',
                    ":del"=>''
                ));
                $_SESSION['msg'] = 'Data Successfully Submitted';
                header('location: index.php');
            } catch (\PDOException $e) {
                echo 'Error:' . $e->getMessage();
            }
        }else {
            header('location: create.php');
        }
    }

}