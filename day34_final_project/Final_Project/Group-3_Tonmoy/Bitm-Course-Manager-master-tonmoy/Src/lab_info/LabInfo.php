<?php

namespace BitmCourseApp\lab_info;

use PDO;

include_once (dirname(__FILE__) . '/../DBConnection/DBConnection.php');

class LabInfo extends \DBConnection {

    public $id, $labno, $course, $seat_capacity, $projector_resulation = '', $ac_status = '', $pc_configuration, $os, $tpc_configuration,
            $table_capacity = '', $internet_speed = '', $data, $error;

    public function prepare($data = '') {
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['lab_no'])) {
            $this->labno = $data['lab_no'];
        }
        if (!empty($data['course_title'])) {
            $this->course = $data['course_title'];
        }
        if (!empty($data['seat_capacity'])) {
            $this->seat_capacity = $data['seat_capacity'];
        }
        if (!empty($data['projector_resolution'])) {
            $this->projector_resulation = $data['projector_resolution'];
        }
        if (!empty($data['ac_status'])) {
            $this->ac_status = $data['ac_status'];
        }
        if (!empty($data['pc_configuration'])) {
            $this->pc_configuration = $data['pc_configuration'];
        }
        if (!empty($data['os'])) {
            $this->os = $data['os'];
        }
        if (!empty($data['trainer_pc_onfiguration'])) {
            $this->tpc_configuration = $data['trainer_pc_onfiguration'];
        }
        if (!empty($data['table_capacity'])) {
            $this->table_capacity = $data['table_capacity'];
        }
        if (!empty($data['internet_speed'])) {
            $this->internet_speed = $data['internet_speed'];
        }
    }

    public function sessionMessage($session_key = '') {
        if (!empty($_SESSION[$session_key]) && isset($_SESSION[$session_key])) {
            echo $_SESSION[$session_key];
            unset($_SESSION[$session_key]);
        }
    }

    public function validation() {
        $required = array(
            'Course_Tilte' => $this->course,
            'Lab_no' => $this->labno,
            'Seat_Capacity' => $this->seat_capacity,
            'Pc_Configuration' => $this->pc_configuration,
            'Operating_System' => $this->os,
            'Trainer_PC_Configuration' => $this->tpc_configuration,
        );

        foreach ($required as $key => $field) {
            if (empty($field)) {
                $_SESSION["$key"] = str_replace('_', ' ', $key) . ' Required';
                $this->error = TRUE;
            }
        }

        if ($this->error == TRUE) {
            $input_value = array(
                'Course_Tilte' => $this->course,
                'Lab_no' => $this->labno,
                'Seat_Capacity' => $this->seat_capacity,
                'Pc_Configuration' => $this->pc_configuration,
                'Operating_System' => $this->os,
                'Trainer_PC_Configuration' => $this->tpc_configuration,
                'Projector_Resualtion' => $this->projector_resulation,
                'Ac_Status' => $this->ac_status,
                'Table_Capacity' => $this->table_capacity,
                'Internet_Speed' => $this->internet_speed,
            );

            foreach ($input_value as $key => $i_value) {
                if (!empty($i_value)) {
                    $_SESSION["v_$key"] = $i_value;
                }
            }
        }
    }

    public function store() {
        if ($this->error == FALSE) {
            try {
                $query = "INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES (NULL, :course , :labno , :seat_capacity , :pro_res , :ac_stat , :pc_con , :os , :t_pc_con , :table_capacity , :int_speed , :created , :updated , :deleted )";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':course' => $this->course,
                    ':labno' => $this->labno,
                    ':seat_capacity' => $this->seat_capacity,
                    ':pro_res' => $this->projector_resulation,
                    ':ac_stat' => $this->ac_status,
                    ':pc_con' => $this->pc_configuration,
                    ':os' => $this->os,
                    ':t_pc_con' => $this->tpc_configuration,
                    ':table_capacity' => $this->table_capacity,
                    ':int_speed' => $this->internet_speed,
                    ':created' => date("Y-m-d h:i:s"),
                    ':updated' => '',
                    ':deleted' => '',
                ));
                $_SESSION['success'] = "New Lab Added";
            } catch (PDOException $e) {
                
            }
        }
        header('location:add_new_lab.php');
    }

    public function viewAllLab($view_by = '') {
        try {
//            if ($view_by == 'running') {
//                $vq = "SELECT * FROM `course_trainer_lab_mapping` WHERE is_running=1 ";
//            } else {
//                $vq = "SELECT * FROM `course_trainer_lab_mapping` WHERE is_running=0 ";
//            }
            $vq = "SELECT * FROM `labinfo` ORDER BY `id` DESC ";
            $view_query = $this->conn->prepare($vq);
            $view_query->execute();
            while ($row = $view_query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
            return $this->data;
        } catch (Exception $ex) {
            
        }
    }

    public function viewLab() {
        try {
            $query = $this->conn->query("SELECT * FROM labinfo WHERE id='$this->id' ");
            $row = $query->fetch();
            return $row;
        } catch (PDOException $ex) {
            
        }
    }

    public function updateLab() {
        if ($this->error == FALSE) {
            try {
                $query = "UPDATE `labinfo` SET `course_id` = :course_id , `lab_no` = :labno , `seat_capacity` = :seat_capa , `projector_resolution` = :projector_reso , `ac_status` = :ac_stat , `pc_configuration` = :pc_con, `os` = :os , `trainer_pc_configuration` = :trainer_pc , `table_capacity` = :table_capa , `internet_speed` = :internet_speed, `updated` = :updated WHERE `labinfo`.`id` = $this->id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':course_id' => $this->course,
                    ':labno' => $this->labno,
                    ':seat_capa' => $this->seat_capacity,
                    ':projector_reso' => $this->projector_resulation,
                    ':ac_stat' => $this->ac_status, //$this->is_active, 
                    ':pc_con' => $this->pc_configuration, //$this->is_admin,
                    ':os' => $this->os, //$this->is_admin,
                    ':trainer_pc' => $this->tpc_configuration, //$this->is_admin,
                    ':table_capa' => $this->table_capacity, //$this->is_admin,
                    ':internet_speed' => $this->internet_speed, //$this->is_admin,
                    ':updated' => date("Y-m-d h:i:s"),
                ));

                $_SESSION['success'] = "Lab Information Updated Successfully.";
                header("location:edit_lab.php?id=$this->id");
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        } else {
            header("location:edit_lab.php?id=$this->id");
        }
    }

}

?>
