<?php
include '../../vendor/autoload.php';

use BitmCourseApp\user\user_login\UserLogin;
use BitmCourseApp\assign_course\AssignCourse;
use BitmCourseApp\lab_info\LabInfo;

$objlogin = new UserLogin();
$objass = new AssignCourse();
$objlab = new LabInfo();

$objlogin->checkLogin();
$objlab->prepare($_GET);
$lab = $objlab->viewLab();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>BITM Course Manager</title>

<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/png" href="../elements/bitm-icon.png"/>
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
<link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="../assets/js/pages/form_layouts.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>
<script type="text/javascript" src="../assets/js/pages/dashboard.js"></script>
<!-- /theme JS files -->
</head>

<body>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="../assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>



        <ul class="nav navbar-nav navbar-right">


            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../elements/user_images/<?php echo $_SESSION['login_confirm']['image']; ?>" width="28" height="28" alt="">
                    <span><?php echo $_SESSION['login_confirm']['username']; ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="../user/user_view_single.php?id=<?php echo $_SESSION['login_confirm']['id']; ?>"><i class="icon-user-plus"></i> My profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="../logout.php"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../elements/user_images/<?php echo $_SESSION['login_confirm']['image']; ?>" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold"><?php echo $_SESSION['login_confirm']['full_name']; ?></span>
                                <div class="text-size-mini text-muted">

                                </div>
                            </div>

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li><a href="../dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li><a href="../user/user_manager.php"><i class="icon-user"></i> <span>User Menu</span></a></li>
                            <li><a href="#"><i class="icon-people"></i> <span>Trainer</span></a></li>
                            <li><a href="#"><i class="icon-book"></i> <span>Course</span></a></li>
                            <li class="active"><a href="lab_info.php"><i class="icon-lan"></i> <span>Lab Info</span></a></li>
                            <li><a href="#"><i class="icon-laptop"></i> <span>Software Management</span></a></li>
                            <li><a href="../assign_course/assign_course.php"><i class="icon-list-unordered"></i> <span>Assign Course</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-lan"></i> - <span class="text-semibold">Lab Information</span></h4>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../dashboard.php"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                        <li><a href="lab_info.php">Lab Information</a></li>
                        <li class="active">Edit Lab Information</li>
                    </ul>
                </div>

            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                
                <!-- Dashboard content -->
                    <div class="panel panel-flat">
                        <div class="navbar navbar-default navbar-xs">
                            <ul class="nav navbar-nav no-border visible-xs-block">
                                <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-down2"></i></a></li>
                            </ul>

                            <div class="navbar-collapse collapse" id="navbar-second">
                                <ul class="nav navbar-nav">
                                    <li class=""><a href="add_new_lab.php">
                                            <i class=" icon-new position-left">
                                            </i> Add New Lab</a>
                                    </li>
                                    <li class=""><a href="view_lab.php?viewBy=running">
                                            <i class=" icon-list-unordered position-left">
                                            </i> View All Lab</a>
                                    </li>
                                    <li class=""><a href="view_lab.php?viewBy=disabled">
                                            <i class=" icon-new-tab2 position-left">
                                            </i> View Disabled Lab</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <form action="lab_update.php" method="post">
                            <div class="panel-flat">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php if (!empty($_SESSION['success'])) { ?>
                                            <div class="alert alert-success alert-styled-left" style="margin: 0 10px 10px 10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                            <?php $objlogin->sessionMessage('success');?>
                                            </div>
                                            <?php } ?>
                                            <fieldset>
                                                <legend class="text-semibold">Edit Lab Information</legend>
                                                <div class="form-group">
                                                    <label>Lad No:</label>
                                                    <input type="text" name="lab_no" class="form-control <?php if(!empty($_SESSION['v_Lab_no']) && $_SESSION['v_Lab_no']!==$lab['lab_no']){echo 'text-blue';}?>" placeholder="Hint. 301" value="<?php
                                                                                    if (!empty($_SESSION['v_Lab_no'])) {
                                                                                        $objlab->sessionMessage('v_Lab_no');
                                                                                    } else {
                                                                                        echo $lab['lab_no'];
                                                                                    } ?>">
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Lab_no')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Course Title:</label>
                                                    <select name="course_title" data-placeholder="Select Course Title" class="select">
                                                        <option value=""></option>
                                                        <?php
                                                        $course_title = $objass->courseTitle();
                                                        foreach ($course_title as $all_course) { ?>
                                                        <option value="<?php echo $all_course['id'];?>" <?php 
                                                        if(isset($_SESSION['v_Course_Tilte']) && !empty($_SESSION['v_Course_Tilte'])) {
                                                            if($_SESSION['v_Course_Tilte']==$all_course['id']) {echo 'selected';unset($_SESSION['v_Course_Tilte']);}
                                                        } elseif ($lab['course_id']==$all_course['id']) {
                                                        echo 'selected';
                                                    }
                                                        ?>><?php echo $all_course['title'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Course_Tilte')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Seat Capacity:</label>
                                                    <input type="text" name="seat_capacity" class="form-control <?php if(!empty($_SESSION['v_Seat_Capacity']) && $_SESSION['v_Seat_Capacity']!==$lab['seat_capacity']){echo 'text-blue';}?>" placeholder="Hint. 30" value="<?php
                                                                                    if (!empty($_SESSION['v_Seat_Capacity'])) {
                                                                                        $objlab->sessionMessage('v_Seat_Capacity');
                                                                                    } else {
                                                                                        echo $lab['seat_capacity'];
                                                                                    } ?>">
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Seat_Capacity')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Projector Resolution:</label>
                                                    <input type="text" name="projector_resolution" class="form-control <?php if(!empty($_SESSION['v_Projector_Resualtion']) && $_SESSION['v_Projector_Resualtion']!==$lab['projector_resolution']){echo 'text-blue';}?>" placeholder="Hint. 1200 × 800" value="<?php
                                                                                    if (!empty($_SESSION['v_Projector_Resualtion'])) {
                                                                                        $objlab->sessionMessage('v_Projector_Resualtion');
                                                                                    } else {
                                                                                        echo $lab['projector_resolution'];
                                                                                    } ?>">
                                                    <label class="validation-error-label"></label>
                                                </div>

                                                <div class="form-group">
                                                    <label>AC Status:</label>
                                                    <select name="ac_status" data-placeholder="Select AC Status" class="select">
                                                        <option value=""></option> 
                                                        <option value="No">No</option> 
                                                        <option>1</option> 
                                                        <option>2</option> 
                                                        <option>3</option>
                                                    </select>
                                                    <label class="validation-error-label"></label>
                                                </div>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend class="text-semibold">.</legend>
                                                <div class="form-group">
                                                    <label>PC Configuration:</label>
                                                    <textarea name="pc_configuration" class="form-control <?php if(!empty($_SESSION['v_Pc_Configuration']) && $_SESSION['v_Pc_Configuration']!==$lab['pc_configuration']){echo 'text-blue';}?>"><?php
                                                                                    if (!empty($_SESSION['v_Pc_Configuration'])) {
                                                                                        $objlab->sessionMessage('v_Pc_Configuration');
                                                                                    } else {
                                                                                        echo $lab['pc_configuration'];
                                                                                    } ?></textarea>
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Pc_Configuration')?></label>
                                                </div>

                                                <div class="form-group">
                                                    <label>Operating System:</label>
                                                    <select name="os" data-placeholder="Select Operating System" class="select">
                                                        <option value=""></option>
                                                        <?php $option = array('Windows','Linux','Duel Boot Windows & Linux','Others');
                                                        foreach ($option as $value) { ?><option <?php 
                                                        if (isset($_SESSION['v_Operating_System']) && !empty($_SESSION['v_Operating_System'])) {
                                                                if ($_SESSION['v_Operating_System'] == $value) {echo 'selected';unset($_SESSION['v_Operating_System']);}
                                                            } elseif ($lab['os']==$value) {
                                                            echo 'selected';
                                                        } ?>><?php echo $value;?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Operating_System')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Trainer PC Configuration:</label>
                                                    <textarea name="trainer_pc_onfiguration" class="form-control <?php if(!empty($_SESSION['v_Trainer_PC_Configuration']) && $_SESSION['v_Trainer_PC_Configuration']!==$lab['trainer_pc_configuration']){echo 'text-blue';}?>"><?php
                                                                                    if (!empty($_SESSION['v_Trainer_PC_Configuration'])) {
                                                                                        $objlab->sessionMessage('v_Trainer_PC_Configuration');
                                                                                    } else {
                                                                                        echo $lab['trainer_pc_configuration'];
                                                                                    } ?></textarea>
                                                    <label class="validation-error-label"><?php $objlab->sessionMessage('Trainer_PC_Configuration')?></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Table Capacity:</label>
                                                    <input type="text" name="table_capacity" class="form-control <?php if(!empty($_SESSION['v_Table_Capacity']) && $_SESSION['v_Table_Capacity']!==$lab['table_capacity']){echo 'text-blue';}?>" placeholder="Hint. 30" value="<?php
                                                                                    if (!empty($_SESSION['v_Table_Capacity'])) {
                                                                                        $objlab->sessionMessage('v_Table_Capacity');
                                                                                    } else {
                                                                                        echo $lab['table_capacity'];
                                                                                    } ?>">
                                                    <label class="validation-error-label"></label>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Internet Speed:</label>
                                                    <input type="text" name="internet_speed" class="form-control <?php if(!empty($_SESSION['v_Internet_Speed']) && $_SESSION['v_Internet_Speed']!==$lab['internet_speed']){echo 'text-blue';}?>" placeholder="Hint. 1 Mbps" value="<?php
                                                                                    if (!empty($_SESSION['v_Internet_Speed'])) {
                                                                                        $objlab->sessionMessage('v_Internet_Speed');
                                                                                    } else {
                                                                                        echo $lab['internet_speed'];
                                                                                    } ?>">
                                                    <label class="validation-error-label"></label>
                                                </div>
                                                <input type="hidden" name="id" value="<?php echo $lab['id'];?>"/>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- /dashboard content -->

                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2016. <a href="#">BASIS Institute of Technology & Management</a> Website <a href="http://www.bitm.org.bd/" target="_blank">BITM.COM</a>
                </div>
                <!-- /footer -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
</body>
</html>
