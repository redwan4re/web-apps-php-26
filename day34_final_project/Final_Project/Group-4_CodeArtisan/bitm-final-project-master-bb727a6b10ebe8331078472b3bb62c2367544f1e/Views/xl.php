<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI=='cli')
    die('This example should only be run from a browser');
require_once '../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../vendor/autoload.php';

use App\course\Course;

$objIndex = new Course();

$allData = $objIndex->ShowAllCourse();
$objPHPExcel=new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSL test Document")
        ->setSubject("Office 2007 XLSL test Document")
        ->setDescription("Test Document for Office 2007 XLSL, generated using PHP Classes.")
        ->setKeywords("Office 2007 openxml php")
        ->setCategory("Test Result file");

$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'SL')
        ->setCellValue('B1', 'ID')
        ->setCellValue('C1', 'Title')
        ->setCellValue('D1', 'Duration')
        ->setCellValue('E1', 'Description');

$counter = 2;
$serial = 0;

foreach($allData as $data){
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $serial)
        ->setCellValue('B'.$counter, $data['id'])
        ->setCellValue('C'.$counter, $data['title'])
        ->setCellValue('D'.$counter, $data['duration'])
        ->setCellValue('E'.$counter, $data['description']);
            $counter++;
}
      $objPHPExcel->getActiveSheet()->setTitle('CourseList');
       $objPHPExcel->setActiveSheetIndex(0);
       
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=1');
header('Expires:Mon, 26 july 1997 05:00:00 GMT');
header('Last Modified: '.gmdate('D, d M Y H:i:s').'GMT');
header('Cache-Control: Cache must revalidate');
header('pragma: public');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php:output');
exit;
        

