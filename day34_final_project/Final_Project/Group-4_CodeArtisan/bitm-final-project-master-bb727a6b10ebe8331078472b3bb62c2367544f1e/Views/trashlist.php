<?php
include_once '../vendor/autoload.php';

use App\course\Course;

$objIndex = new Course();

$allData = $objIndex->ShowAllCourse();
$i = 1;
$serial = 1;
?>

<?php include_once 'header.php'; ?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">List</span> Of Disabled Courses</h4>
        </div><?php $objIndex->sessionMsg('deleteSuccess') ?>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-columned footable" data-page-size="10"
                                       data-next-text="Next" data-previous-text="Previous">
                                    <thead>
                                        <tr class="bg-teal-600">
                                            <th>#</th>
                                            <th>Course<br>Name</th>
                                            <th>Duration</th>
                                            <th>Type</th>
                                            <th>Created</th>
                                            <th>Modified</th>
                                            <th>Delete</th>
                                            <th colspan="2">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($allData as $singleData) {
                                            if ($singleData['is_delete'] == 0) {
                                                continue;
                                            }
                                            ?>
                                            <tr class="<?php
                                            if ($i % 2 == 0) {
                                                echo "info";
                                            } else {
                                                echo "alpha-slate";
                                            }
                                            $i++;
                                            ?>">
                                                <td><?php echo $serial++; ?></td>
                                                <td><?php echo $singleData['title']; ?></td>
                                                <td><?php
                                                    if ($singleData['duration'] == '15_days') {
                                                        echo "15 Days ";
                                                    }
                                                    if ($singleData['duration'] == '1_month') {
                                                        echo "1 Month";
                                                    }
                                                    if ($singleData['duration'] == '2_months') {
                                                        echo "2 Months";
                                                    }
                                                    if ($singleData['duration'] == '3_months') {
                                                        echo "3 Months";
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                <?php
                                                if ($singleData['course_type'] == 0) {
                                                    echo '<b><span class="text-danger">FREE Course</span></b>';
                                                } else {
                                                    echo 'Paid Course';
                                                }if ($singleData['is_offer'] == 0) {
                                                        echo '<br><span class="badge bg-danger-400">Offered</span>';
                                                }
                                                ?>
                                                </td>
                                                <td><?php echo $singleData['created']; ?>
                                                </td>
                                                <td><?php
                                                    if ($singleData['updated'] == '0000-00-00 00:00:00') {
                                                        echo "Not Modify Yet";
                                                    } else {
                                                        echo $singleData['updated'];
                                                    }
                                                    ?></td>
                                                <td><?php
                                                    if ($singleData['deleted'] == '0000-00-00 00:00:00') {
                                                        echo "Not Attempt to Delete Yet";
                                                    } else {
                                                        echo $singleData['updated'];
                                                    }
                                                    ?></td>
                                                <td class="text-center">
                                                    
                                                    <a class="btn border-teal text-teal-800 btn-flat" type="button" href="restore.php?id=<?php echo $singleData['unique_id']; ?>">Restore</a>
                                                </td>   
                                                <td>
                                                    <a class="btn border-teal text-teal-800 btn-flat" type="button" href="delete.php?id=<?php echo $singleData['unique_id']; ?>">Delete
                                                </td>
                                            </tr>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>

                                    <!-- Pagination -->
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="3">
                                                <div class="pagination text-center"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <!-- /Pagination -->

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /horizotal form -->

                </div>

            </div>
        </div>
        <!-- /grid -->

    </div>
</div>
<!-- /detached content -->
<?php include_once 'footer.php';?>