<?php
include_once '../vendor/autoload.php';

use App\course\Course;

$obj = new Course();

$obj->prepare($_GET);

$oneData = $obj->show();

if (!empty( $oneData) && isset( $oneData)) {

    include_once 'header.php';
    ?>
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Detais of</span> Of <?php echo $oneData['title']; ?> Course</h4>
            </div>


        </div>

    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Detached sidebar -->
        <?php include_once 'sidebar.php'; ?>
        <!-- /detached sidebar -->


        <!-- Detached content -->
        <div class="container-detached">
            <div class="content-detached">

                <!-- Grid -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Horizontal form -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h3 class="panel-title">Course Description </h3>
                                <div class="heading-elements">
                                    <div class="heading-btn-group">
                                        <a type="button" class="btn bg-teal btn-labeled" href="edit.php?id=<?php echo $oneData['unique_id']; ?>"><b><i class="icon-pencil7"></i></b> Edit Course</a>
                                        <a href="trash.php?id=<?php echo $oneData['unique_id']; ?>" class="btn bg-teal btn-labeled" onclick="return confirm('Are you sure you want to disable this course?');"><b><i class="icon-close2 position-left"></i></b>
                                                        Disable Course
                                        </a>
                                    </div>
                                </div>
                            </div>




                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr class="success">
                                                <th class = "col-md-3"><b>ID</b></th>
                                                <td class = "col-md-9"><?php echo $oneData['id']; ?></td>
                                            </tr>
                                            <tr class="warning">
                                                <th class = "col-md-3"><b>Course Name</b></th>
                                                <td class = "col-md-9"><?php echo $oneData['title']; ?></td>
                                            </tr>
                                            <tr class="success">
                                                <th class = "col-md-3"><b>Duration</b></th>
                                                <td class = "col-md-9">
                                                    <?php
                                                    if ($oneData['duration'] == '15_days') {
                                                        echo "15 Days";
                                                    }
                                                    if ($oneData['duration'] == '1_month') {
                                                        echo "1 Month";
                                                    }
                                                    if ($oneData['duration'] == '2_months') {
                                                        echo "2 Months";
                                                    }
                                                    if ($oneData['duration'] == '3_months') {
                                                        echo "3 Months";
                                                    }
                                                    ?></td>
                                            </tr>
                                            <tr class="warning">
                                                <th class = "col-md-3"><b>Type</b></th>
                                                <td class = "col-md-9">
                                                    <?php
                                                    if ($oneData['course_type'] == 0) {
                                                        echo '<b><span class="text-danger">FREE Course</span></b>';
                                                    } else {
                                                        echo 'Paid Course';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr class="success">
                                                <th class = "col-md-3"><b>Course Fee</b></th>
                                                <td class = "col-md-9">
                                                    <?php
                                                    if ($oneData['course_fee'] == 0) {
                                                        echo'Not Applicable';
                                                    } else {
                                                        echo $oneData['course_fee'];
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($oneData['is_offer'] == 0) {
                                                        echo '<br><span class="badge bg-danger-400">Offered</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr class="warning">
                                                <th class = "col-md-3"><b>Description</b></th>
                                                <td class = "col-md-9"><div class="more_no">
                                                        <?php echo $oneData['description']; ?></div>
                                                </td>
                                            </tr>
                                            <tr class="success">
                                                <th class = "col-md-3"><b>Created</b></th>
                                                <td class = "col-md-9"><?php echo $oneData['created']; ?></td>
                                            </tr>
                                            <tr class="warning">
                                                <th class = "col-md-3"><b>Modified</b></th>
                                                <td class = "col-md-9">
                                                    <?php
                                                    if ($oneData['updated'] == '0000-00-00 00:00:00') {
                                                        echo "No Modify Yet";
                                                    } else {
                                                        echo $oneData['updated'];
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr class="success">
                                                <th class = "col-md-3"><b>Delete</b></th>
                                                <td class = "col-md-9">
                                                    <?php
                                                    if ($oneData['deleted'] == '0000-00-00 00:00:00') {
                                                        echo "No Delete Yet";
                                                    } else {
                                                        echo $oneData['deleted'];
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /horizotal form -->
                    </div>

                </div>
            </div>
            <!-- /grid -->

        </div>
    </div>
    <!-- /detached content -->
    <?php
    include_once 'footer.php';
} else {
    $_SESSION['errorMsg'] = "Sorry Wrong Url";
    header('location:error.php');
}
?>