<?php
function activeClass($path = "")
{

    if (basename($_SERVER['REQUEST_URI'], '.php') == $path) {
        echo '<span class="label bg-warning-400">Current</span>';
    }
}

?>

<div class="sidebar-detached">
    <div class="sidebar sidebar-default">
        <div class="sidebar-content">

            <!-- Sidebar search -->
            <div class="sidebar-category">
                <div class="category-title">
                    <span>Course Search</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <!-- course search code -->
                <div class="category-content">
                    <form action="search.php" method="post">
                        <div class="has-feedback has-feedback-left">
                            <input type="text" name="var1" class="form-control" placeholder="Search for courses...">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base text-muted"></i>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /sidebar search -->


            <!-- Sub navigation -->
            <div class="sidebar-category">
                <div class="category-title">
                    <span>Navigation</span>
                    <ul class="icons-list">
                        <li><a href="#" data-action="collapse"></a></li>
                    </ul>
                </div>

                <div class="category-content no-padding">
                    <ul class="navigation navigation-alt navigation-accordion">
                        <li>
                            <a href="#"><i class="icon-cog3"></i> User Info</a>
                            <ul>
                                <li><a href="#">Registration</a></li>
                                <li>
                                    <a href="#"> Login</a>
                                </li>
                                <li><a href="#"> Profile</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="icon-cog3"></i> Trainer Info</a>
                            <ul>
                                <li><a href="#"> Trainer List</a></li>
                                <li>
                                    <a href="#"> Team</a>
                                </li>
                                <li><a href="#"> Trainer Entry</a></li>
                            </ul>
                        </li>


                        <li>
                            <a href="#"><i class="icon-cog3"></i> Courses Info</a>
                            <ul>
                                <li><a href="create.php">Course Entry<?php activeClass('create') ?></a></li>
                                <li>
                                    <a href="#"> Course List<?php activeClass('index') ?></a>
                                    <ul>
                                        <li><a href="index.php"> All Course</a></li>
                                        <li><a href="#"> Free Course</a></li>
                                        <li>
                                            <a href="#"> Paid Course</a>
                                            <ul>
                                                <li><a href="#"> Below 5000 Tk</a></li>
                                                <li><a href="#"> Above 5000 Tk</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"> Offered Courses</a></li>
                                    </ul>
                                </li>
                                <li><a href="trashlist.php"> Disabled courses<?php activeClass('trashlist') ?></a></li>
                                <li><a href="pdf.php"> As pdf</a></li>
                                <li><a href="xl.php"> As Excel</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="icon-cog3"></i> Lab Info</a>
                            <ul>
                                <li><a href="#">Lab List</a></li>
                                <li>
                                    <a href="#"> Lab Configuration</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="icon-cog3"></i> Software Info</a>
                            <ul>
                                <li><a href="#">Software List</a></li>
                                <li>
                                    <a href="#"> Software Configuration</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /sub navigation -->
        </div>
    </div>
</div>