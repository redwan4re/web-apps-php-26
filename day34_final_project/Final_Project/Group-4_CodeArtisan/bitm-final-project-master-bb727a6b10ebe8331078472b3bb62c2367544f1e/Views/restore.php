<?php
include_once '../vendor/autoload.php';

use App\course\Course;

$objRestore = new Course();

$objRestore->prepare($_GET) -> restore();