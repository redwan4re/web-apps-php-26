<?php
include '../vendor/mpdf/mpdf/mpdf.php';
include '../vendor/autoload.php';
use App\course\Course;

$objIndex = new Course();

$allData = $objIndex->ShowAllCourse();
$trs="";
$serial=0;
foreach($allData as $data):
$serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$data['id']."</td>";
$trs.="<td>".$data['title']."</td>";
$trs.="<td>".$data['duration']."</td>";
$trs.="<td>".$data['description']."</td>";
$trs.="</tr>";
endforeach;
$html = <<<EOD
<!DOCTYPE html>
<html>
   <head>
        <title>COURSE LIST PDF FORMAT</title>
        </head>
        <body>
        <h1>LIST OF ALL COURSES</h1>
        <table border="1">
        <thead>
        <tr>
            <th>Sl.</th>   
             <th>ID</th>
                <th>Title</th>
                <th>Duration</th>
                <th>Description</th>
            </tr>
            </thead>
        <tbody>
        $trs;
        
        </tbody>
        </table>
EOD;
        
$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->output();
exit;
?>

        

