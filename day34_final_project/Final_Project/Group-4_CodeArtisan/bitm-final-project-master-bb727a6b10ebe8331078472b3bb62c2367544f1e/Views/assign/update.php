<?php

include_once "../../vendor/autoload.php";

use App\assign\Assign;

$objUpdate = new Assign();
//
$prepare = $objUpdate->prepare($_POST);
//$objUpdate->validation();
//$objUpdate->store();
$allTrainers = $objUpdate->AllTrainers();
//
$allLabs = $objUpdate->AllLabs();

$_SESSION['EditPost'] = $_POST;



$postId =  $_POST['id'];

 $locationCange = "location:edit.php?id=$postId";



if (!empty($_POST['course_id'])) {
    $valid = TRUE;

    if (!empty($_POST['leadTrainer'])) {

        foreach ($allTrainers as $trainer) {
            if ($_POST['leadTrainer'] == $trainer['full_name']) {
                if ($trainer['courses_id'] == $_POST['course_id']) {
                    $valid = TRUE;


                    if (!empty($_POST['labAssist'])) {

                        foreach ($allTrainers as $trainer) {
                            if ($_POST['labAssist'] == $trainer['full_name']) {
                                if ($trainer['courses_id'] == $_POST['course_id']) {
                                    $valid = TRUE;



                                    if (!empty($_POST['batch'])) {
                                        $valid = TRUE;

                                        if (!empty($_POST['assistTrainer'])) {

                                            $valid = TRUE;

                                            if (!empty($_POST['lab_num'])) {
                                                $valid = TRUE;

                                                if (!empty($_POST['start_date'])) {
                                                    $valid = TRUE;

                                                    if (!empty($_POST['start_time'])) {
                                                        $valid = TRUE;

                                                        if (!empty($_POST['end_date'])) {
                                                            $valid = TRUE;
                                                            if (!empty($_POST['end_time'])) {
                                                                $valid = TRUE;

                                                                if ($valid == TRUE) {
                                                                    $objUpdate->update();
                                                                } else {
                                                                    $_SESSION['DataError'] = '<h4 class= "text-info">Sorry data not stored to Database, please try again</h4>';
                                                                    header("$locationCange");
                                                                }
                                                            } else {
                                                                $valid = FALSE;
                                                                $_SESSION['end_timeError'] = "End Time is required";
                                                                header("$locationCange");
                                                            }// End Time Validation is complete
                                                        } else {
                                                            $valid = FALSE;
                                                            $_SESSION['end_dateError'] = "End Date is required";
                                                            header("$locationCange");
                                                        }// End Date Validation is complete
                                                    } else {
                                                        $valid = FALSE;
                                                        $_SESSION['start_timeError'] = "Start Time is required";
                                                        header("$locationCange");
                                                    }// Start Time Validation is complete
                                                } else {
                                                    $valid = FALSE;
                                                    $_SESSION['start_dateError'] = "Start Date is required";
                                                    header("$locationCange");
                                                }// Start Date Validation is complete
                                            } else {
                                                $valid = FALSE;
                                                $_SESSION['labNumberError'] = "Batch Number is required";
                                                header("$locationCange");
                                            }// Lab Assistant Validation is complete
                                        } else {
                                            $valid = FALSE;
                                            $_SESSION['assistTrainerError'] = "Assistant trainer is requiered";
                                            header("$locationCange");
                                        }// Assistant Trainer Validation is complete
                                    } else {
                                        $valid = FALSE;
                                        $_SESSION['batchNumberError'] = "Batch Number is required";
                                        header("$locationCange");
                                    }// Batch Number Validation is complete
                                } else {
                                    $valid = FALSE;
                                    $_SESSION['labAssistError'] = "Please select correct Lab Assistant";
                                    header("$locationCange");
                                }
                            }
                        }
                    } else {
                        $valid = FALSE;
                        $_SESSION['labAssistError'] = "Lab Assistant requiered";
                        header("$locationCange");
                    }// Lab Assistant Validation is complete
                } else {
                    $valid = FALSE;
                    $_SESSION['leadTrainerError'] = "Please select correct lead trainer";
                    header("$locationCange");
                }
            }
        }
    } else {
        $valid = FALSE;
        $_SESSION['leadTrainerError'] = "Lead Trainer requiered";
        header("$locationCange");
    }// Lead Trainer Validation is complete
} else {
    $valid = FALSE;
    $_SESSION['emptyCourse'] = "Please first fill the course title";
    header("$locationCange");
}
?>