<?php

include_once "../../vendor/autoload.php";

use App\assign\Assign;

$objStore = new Assign();
//
$prepare = $objStore->prepare($_POST);
//$objStore->validation();
//$objStore->store();
$allTrainers = $objStore->AllTrainers();
//
$allLabs = $objStore->AllLabs();

$allDatas = $objStore->allData();

$_SESSION['POST'] = $_POST;
print_r($_POST);

if (!empty($_POST['course_id'])) {
    $valid = TRUE;

    if (!empty($_POST['leadTrainer'])) {

        foreach ($allTrainers as $trainer) {
            if ($_POST['leadTrainer'] == $trainer['full_name']) {
                if ($trainer['courses_id'] == $_POST['course_id']) {
                    $valid = TRUE;


                    if (!empty($_POST['labAssist'])) {

                        foreach ($allTrainers as $trainer) {
                            if ($_POST['labAssist'] == $trainer['full_name']) {
                                if ($trainer['courses_id'] == $_POST['course_id']) {
                                    $valid = TRUE;



                                    if (!empty($_POST['batch'])) {
                                        $valid = TRUE;

                                        if (!empty($_POST['assistTrainer'])) {

                                            $valid = TRUE;

                                            if (!empty($_POST['lab_num'])) {
                                                $valid = TRUE;

                                                if (!empty($_POST['start_date'])) {
                                                    $valid = TRUE;

                                                    if (!empty($_POST['start_time'])) {
                                                        $valid = TRUE;

                                                        if (!empty($_POST['end_date'])) {
                                                            $valid = TRUE;
                                                            if (!empty($_POST['end_time'])) {
                                                                $valid = TRUE;

                                                                if ($valid == TRUE) {
                                                                    $objStore->store();
                                                                } else {
                                                                    $_SESSION['DataError'] = '<h4 class= "text-info">Sorry data not stored to Database, please try again</h4>';
                                                                    header('location:index.php');
                                                                }
                                                            } else {
                                                                $valid = FALSE;
                                                                $_SESSION['end_timeError'] = "End Time is required";
                                                                header('location:index.php');
                                                            }// End Time Validation is complete
                                                        } else {
                                                            $valid = FALSE;
                                                            $_SESSION['end_dateError'] = "End Date is required";
                                                            header('location:index.php');
                                                        }// End Date Validation is complete
                                                    } else {
                                                        $valid = FALSE;
                                                        $_SESSION['start_timeError'] = "Start Time is required";
                                                        header('location:index.php');
                                                    }// Start Time Validation is complete
                                                } else {
                                                    $valid = FALSE;
                                                    $_SESSION['start_dateError'] = "Start Date is required";
                                                    header('location:index.php');
                                                }// Start Date Validation is complete
                                            } else {
                                                $valid = FALSE;
                                                $_SESSION['labNumberError'] = "Batch Number is required";
                                                header('location:index.php');
                                            }// Lab Assistant Validation is complete
                                        } else {
                                            $valid = FALSE;
                                            $_SESSION['assistTrainerError'] = "Assistant trainer is requiered";
                                            header('location:index.php');
                                        }// Assistant Trainer Validation is complete
                                    } else {
                                        $valid = FALSE;
                                        $_SESSION['batchNumberError'] = "Batch Number is required";
                                        header('location:index.php');
                                    }// Batch Number Validation is complete
                                } else {
                                    $valid = FALSE;
                                    $_SESSION['labAssistError'] = "Please select correct Lab Assistant";
                                    header('location:index.php');
                                }
                            }
                        }
                    } else {
                        $valid = FALSE;
                        $_SESSION['labAssistError'] = "Lab Assistant requiered";
                        header('location:index.php');
                    }// Lab Assistant Validation is complete
                } else {
                    $valid = FALSE;
                    $_SESSION['leadTrainerError'] = "Please select correct lead trainer";
                    header('location:index.php');
                }
            }
        }
    } else {
        $valid = FALSE;
        $_SESSION['leadTrainerError'] = "Lead Trainer requiered";
        header('location:index.php');
    }// Lead Trainer Validation is complete
} else {
    $valid = FALSE;
    $_SESSION['emptyCourse'] = "Please first fill the course title";
    header('location:index.php');
}
?>