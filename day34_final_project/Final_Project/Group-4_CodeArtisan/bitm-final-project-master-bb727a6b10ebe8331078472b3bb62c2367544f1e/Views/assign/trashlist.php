<?php
include_once "../../vendor/autoload.php";

use App\assign\Assign;

$obj = new Assign();

$allTrainers = $obj->AllTrainers();
//
$allLabs = $obj->AllLabs();

$allDatas = $obj->allData();

$allData = $obj->trashlist();

if (!empty($_SESSION['EditPost']) && isset($_SESSION['EditPost'])) {
    session_unset($_SESSION['EditPost']);
}


$i = 1;
$p = 1;
$serial = 1;
?>

<?php include_once 'header.php'; ?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Disabled</span> Sesion List</h4>
        </div><?php $obj->sessionMsg('deleteSuccess') ?>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-columned footable" data-page-size="10"
                                       data-next-text="Next" data-previous-text="Previous">
                                    <thead>
                                        <tr class="disabled bg-teal-600">
                                            <th>#</th>
                                            <th>Course ID</th>
                                            <th>Batch_no</th>
                                            <th>Lead<br>Trainer</th>
                                            <th>Assist<br>Trainer</th>
                                            <th>Lab<br>Num</th>
                                            <th>Day</th>
                                            <th class="text-center" colspan="2">Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($allData) && isset($allData)) {

                                            foreach ($allData as $singleData) {
//                                            if ($singleData['is_running'] == 1) {
//                                                continue;
//                                            }
                                                ?>
                                                <tr class="<?php
                                                if ($i % 2 == 0) {
                                                    echo "info";
                                                } else {
                                                    echo "alpha-slate";
                                                }
                                                $i++;
                                                ?>">
                                                    <td><?php echo $serial++; ?></td>
                                                    <td><?php echo $singleData['course_id'] ?>
                                                    </td>
                                                    <td><?php echo $singleData['batch_no']; ?></td>
                                                    <td><?php echo $singleData['lead_trainer']; ?></td>
                                                    <td><?php echo $singleData['asst_trainer']; ?></td>
                                                    <td><?php echo $singleData['lab_id']; ?></td>
                                                    <td><?php
                                                        if ($singleData['day'] == 'day1') {
                                                            echo 'Sat-Mon-Wed sesseion';
                                                        }
                                                        if ($singleData['day'] == 'day2') {
                                                            echo 'Sun-Tue-Thu sesseion';
                                                        }
                                                        if ($singleData['day'] == 'day3') {
                                                            echo 'Friday sesseion';
                                                        }
                                                        ?></td>
                                                    <td class="text-center">
                                                        <!--                                                    <div class="btn-group">
                                                        <button class="btn btn-primary left" type="button" href="edit.php?id=<?php echo $singleData['id']; ?>">Left</button>
                                                        <button class="btn btn-primary left" type="button" href="edit.php?id=<?php echo $singleData['id']; ?>">Left</button></div>-->
                                                        <div class="btn-group btn-group-justified">
                                                            <a class="btn bg-teal-800" href="restore.php?id=<?php echo $singleData['id']; ?>">RESTORE</a>
                                                            <a class="btn bg-teal-800" onclick="return confirm('Are you sure you want to Delete this course session?');" href="delete.php?id=<?php echo $singleData['id']; ?>">DELETE</a>
                                                        </div>
                                                    </td>

                                                    <?php
                                                }
                                                ?>
                                                <?php
                                            } else {
                                                echo '<td class="info text--center" colspan = "9"><h3 class="text-center">No Data Available</h3></td>';
                                            }
                                            ?>
                                    </tbody>

                                    <!-- Pagination -->
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="3">
                                                <div class="pagination text-center"></div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                    <!-- /Pagination -->

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /horizotal form -->


                </div>

            </div>
        </div>

    </div> </div>
</div>
<!-- /detached content -->
<?php include_once 'footer.php'; ?>

