<?php
include_once '../vendor/autoload.php';

use App\course\Course;

$obj = new Course();
?>

<?php include_once 'header.php'; ?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Create</span> - Courses Module</h4>
        </div><?php $obj->sessionMsg('storeSuccess') ?>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Detached sidebar -->
    <?php include_once 'sidebar.php'; ?>
    <!-- /detached sidebar -->


    <!-- Detached content -->
    <div class="container-detached">
        <div class="content-detached">

            <!-- Grid -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal form -->
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h3 class="panel-title">Course Entry </h3>
                        </div>

                        <div class="panel-body">
                            <form class="form-horizontal" action="store.php" method="POST">
                                <!-- /Course Name -->

                                <!-- Course Duration -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Course Duration</label>
                                    <div class="col-lg-5">
                                        <select name="duration" class="form-control">
                                            <option selected>Select Here</option>
                                            <option value="15_days" <?php
                                            if (isset($_SESSION['course_duration_V'])) {
                                                echo ($_SESSION['course_duration_V'] == "15_days") ?
                                                        'selected' : '';
                                            }
                                            ?>
                                                    >15 Days
                                            </option>
                                            <option value="1_month" <?php
                                            if (isset($_SESSION['course_duration_V'])) {
                                                echo ($_SESSION['course_duration_V'] == "1_month") ?
                                                        'selected' : '';
                                            }
                                            ?>>1 Month
                                            </option>
                                            <option value="2_months" <?php
                                            if (isset($_SESSION['course_duration_V'])) {
                                                echo ($_SESSION['course_duration_V'] == "2_months") ?
                                                        'selected' : '';
                                            }
                                            ?>>2 Months
                                            </option>
                                            <option value="3_months" <?php
                                            if (isset($_SESSION['course_duration_V'])) {
                                                echo ($_SESSION['course_duration_V'] == "3_months") ?
                                                        'selected' : '';
                                            }
                                            ?>>3 Months</option>
                                        </select>
                                        <span class="help-block">Course Duration is required</span>
                                        <span
                                            class="text-danger"><?php $obj->sessionMsg("course_duration_M"); ?></span>
                                    </div>
                                </div>
                                <!-- /Course Duration -->

                                <!-- Course type -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Course Type</label>
                                    <div class="col-lg-10">
                                        <div class="col-md-6 col-lg-6" style="padding-left: 0px!important">
                                            <div class="form-group">
                                                <select id="ctype" onchange="showHide()" name="course_type"
                                                        class="form-control">
                                                    <option selected disabled hidden>Select Here</option>
                                                    <option value='2'<?php
                                                    if (isset($_SESSION['course_type_V'])) {
                                                        echo ($_SESSION['course_type_V'] == '0') ?
                                                                'selected' : '';
                                                    }
                                                    ?>>Free Course</option>
                                                    <option value='1'<?php
                                                    if (isset($_SESSION['course_type_V'])) {
                                                        echo ($_SESSION['course_type_V'] == '1') ?
                                                                'selected' : '';
                                                    }
                                                    ?>>Paid Course</option>
                                                </select>
                                                <span class="help-block">Course Type is required</span>
                                                <span
                                                    class="text-danger"><?php $obj->sessionMsg("course_type_M"); ?></span>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <input type="number" name="course_fee" class="form-control" placeholder="Insert course price" value="<?php $obj->sessionMsg("course_fee_V"); ?>">
                                                <span class="help-block">Course Price is required if it is paid course</span>
                                                <span
                                                    class="text-danger"><?php $obj->sessionMsg("course_fee_M"); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Course type -->

                                <!-- Course offer -->
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Course Offer:</label>
                                    <div class="col-lg-10">
                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="is_offer" value=0>
                                            Course in offer
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" class="styled" name="is_offer" checked="checked" value=1>
                                            Course is <b>not</b> in offer
                                        </label>
                                    </div>
                                </div>
                                <!-- /Course offer -->

                                <!-- Course Description -->
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Course Description</label>
                                    <div class="col-lg-10">
                                        <textarea rows="5" cols="5" class="form-control" name="description"
                                                  placeholder="Write description about that Courses"></textarea>
                                    </div>
                                </div>
                                <!-- /Course Description -->

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Submit form <i
                                            class="icon-arrow-right14 position-right"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /horizotal form -->

                </div>

            </div>
        </div>
        <!-- /grid -->

    </div>
</div>
<!-- /detached content -->

<?php include_once 'footer.php'; ?>
