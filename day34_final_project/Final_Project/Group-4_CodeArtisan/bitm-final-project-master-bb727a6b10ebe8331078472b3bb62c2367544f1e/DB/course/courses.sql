-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 16, 2016 at 06:10 পূর্বাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(23, '57ad650a0fb33', 'Digital Marketing', '3_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Digital Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.\r\n', '2', '0', 1, 1, '2016-08-12 07:56:26', '2016-08-15 09:24:25', '2016-08-15 09:47:25'),
(24, '57ad652c3e71f', 'Web Application Development- Dot Net', '3_months', 'After completing this training program, the trainee will be able to work as a professional web application developer in a local market as well as offshore market place.', '1', '15000', 1, 1, '2016-08-12 07:57:00', '0000-00-00 00:00:00', '2016-08-15 09:48:21'),
(25, '57ad6555d741f', 'Web Application Development- PHP', '3_months', 'After completing this training program, the trainee will be able to work as a professional web application developer in a local market as well as offshore market place.', '1', '2000', 1, 1, '2016-08-12 07:57:41', '2016-08-15 09:20:27', '2016-08-15 09:56:58'),
(26, '57ad657098b53', 'Practical SEO', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Internet Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.', '2', '0', 1, 1, '2016-08-12 07:58:08', '2016-08-15 09:34:56', '2016-08-15 09:58:29'),
(27, '57ad6599a62a7', 'Affiliate Marketing  & E-commerce', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Internet Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.', '2', '0', 0, 0, '2016-08-12 07:58:49', '2016-08-15 10:18:22', '2016-08-15 07:58:06'),
(28, '57ad65ba7ecd2', ' Mobile Application Development-Android', '3_months', 'Opportunities are same in the local market as well as offshore market to work as a mobile application developer.', '1', '20000', 1, 0, '2016-08-12 07:59:22', '0000-00-00 00:00:00', '2016-08-13 10:27:07'),
(29, '57ad65f34df45', 'Server Management and Cloud Management', '2_months', 'After completing this training program, the trainee will have a proper knowledge on Digital Marketing and will able to work any organization as Digital Marketing Professional. Or, he can choose his profession as a freelancer and can work in outsourcing marketplace.\r\n', '1', '20000', 1, 0, '2016-08-12 08:00:19', '0000-00-00 00:00:00', '2016-08-15 09:47:11'),
(30, '57ad661fec9de', 'IT support Technical', '2_months', 'After completing this training program, the trainee will have a proper knowledge on IT support Technical and will able to work any organization.', '2', '0', 0, 0, '2016-08-12 08:01:03', '0000-00-00 00:00:00', '2016-08-15 09:54:59'),
(31, '57ad6739e1ba0', 'Customer Support & Service', '1_month', 'After completing this program, the trainee will be able to work in any local company (Call Centre, Telecommunication & IT company) as customer support executive.', '1', '5000', 1, 0, '2016-08-12 08:05:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '57af674fe74ce', 'Software Design/Architecture', '3_months', 'After completion, participants will understand OOP design principles, software design & architecture. They will learn to recognize code smells and refactoring for improving code quality. They will obtain an excellent understanding of several design principles and design patterns.', '1', '12000', 1, 0, '2016-08-13 08:30:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '57af75193a92d', 'JavaScript Fundamental ', '1_month', 'After completion of this course, students will gain:\r\nSolid foundation on programming techniques using JavaScriptUnderstanding on graceful degradation, browser compatibility, unobtrusive JavaScript   that give insight of modern librariesKnowledge on jQuery-less world of JavaScript which ultimately help to learn jQuery (and any other library) better', '1', '5000', 1, 0, '2016-08-13 09:29:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '57af75dece424', 'Android Application Development', '3_months', 'This course is designed for those who are trying to build their career in the android apps development arena. Participants will be trained on real life time projects, technology tools and methodologies that are very much relevant to Android Application Development. By the end of this course you will have a comprehensive knowledge of developing professional android apps using java language. You will be able to use Eclipse IDE with SDK', '2', '0', 0, 0, '2016-08-13 09:32:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '57af777d87965', 'Managing Software Projects', '2_months', 'Mid-level training; 3 or 4 yearsâ€™ experience as Software Engineer. One will practically learn all mentioned tools and techniques by completing a project through this practical training course.', '2', '0', 1, 0, '2016-08-13 09:39:41', '0000-00-00 00:00:00', '2016-08-13 11:21:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
