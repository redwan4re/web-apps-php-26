<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/7/2016
 * Time: 5:04 PM
 */
session_start();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BITM Lab Management_by Muhammad Redwan_SEIP-122515</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="../../index.php">BITM Lab Management</a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>


        </ul>

        <!--<p class="navbar-text"><span class="label bg-success-400">Online</span></p>-->

        <ul class="nav navbar-nav navbar-right">


            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="../../assets/images/placeholder.jpg" alt="">
                    <span>BITM</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="../../assets/images/placeholder.jpg"
                                                                class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">BITM</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp; Dhaka Campus
                                </div>
                            </div>

                            <!--<div class="media-right media-middle">-->
                            <!--<ul class="icons-list">-->
                            <!--<li>-->
                            <!--<a href="#"><i class="icon-cog3"></i></a>-->
                            <!--</li>-->
                            <!--</ul>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i>
                            </li>
                            <li><a href="../../index.php"><i class="icon-home4"></i>
                                    <span>Dashboard</span></a></li>


                            <li class="active">
                                <a href="user_main.php"><i class="icon-user"></i> <span>Users</span></a>

                            </li>

                            <li>
                                <a href="#"><i class="icon-people"></i> <span>Trainer</span></a>
                                <ul>
                                    <li><a href="starters/horizontal_nav.html">Add Trainer</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-book"></i> <span>Course Info</span></a>
                                <ul>
                                    <li><a href="starters/horizontal_nav.html">Add Trainer</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-laptop"></i> <span>Lab Info</span></a>
                                <ul>
                                    <li><a href="starters/horizontal_nav.html">Add Trainer</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-cube4"></i> <span>Software Info</span></a>
                                <ul>
                                    <li><a href="starters/horizontal_nav.html">Add Trainer</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="icon-stack"></i> <span>Assign Course</span></a>
                                <ul>
                                    <li><a href="starters/horizontal_nav.html">Add Trainer</a></li>

                                </ul>
                            </li>
                            <!-- /main -->


                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"></span>
                            Add User</h4>
                        <?php if (isset($_SESSION ['submit_msg']) && !empty($_SESSION ['submit_msg'])) {
                        echo $_SESSION ['submit_msg'];
                        unset($_SESSION ['submit_msg']);
                        }
                        ?>

                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            <a href="#" class="btn btn-link btn-float has-text"><i
                                    class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                            <!--<a href="#" class="btn btn-link btn-float has-text"><i-->
                            <!--class="icon-calculator text-primary"></i> <span>Invoices</span></a>-->
                            <!--<a href="#" class="btn btn-link btn-float has-text"><i-->
                            <!--class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>-->
                        </div>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Add User</li>
                    </ul>

                    <!--<ul class="breadcrumb-elements">-->
                    <!--<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>-->
                    <!--<li class="dropdown">-->
                    <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                    <!--<i class="icon-gear position-left"></i>-->
                    <!--Settings-->
                    <!--<span class="caret"></span>-->
                    <!--</a>-->

                    <!--<ul class="dropdown-menu dropdown-menu-right">-->
                    <!--<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>-->
                    <!--<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>-->
                    <!--<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>-->
                    <!--<li class="divider"></li>-->
                    <!--<li><a href="#"><i class="icon-gear"></i> All settings</a></li>-->
                    <!--</ul>-->
                    </li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Content area -->
                <div class="content">

                    <!-- Horizontal form options -->
                    <div class="row">
                        <div class="col-md-12">

                            <!-- Basic layout-->
                            <form action="store.php" method="POST" class="form-horizontal"
                                  enctype="multipart/form-data">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h5 class="panel-title" style="color: #26A69A;">Add New User</h5>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <!--                                                <li><a data-action="reload"></a></li>-->
                                                <!--                                                <li><a data-action="close"></a></li>-->

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"> <b>Full Name:</b></label>
                                            <div class="col-lg-7">
                                                <input type="text" name="full_name" class="form-control"
                                                       placeholder="Enter full name here (6-30 characters)">
                                                <?php
                                                if (!empty($_SESSION['name_err'])) {
                                                    echo $_SESSION['name_err'];
                                                    unset($_SESSION['name_err']);
                                                }

                                                if (!empty($_SESSION['name_err2'])) {
                                                    echo $_SESSION['name_err2'];
                                                    unset($_SESSION['name_err2']);
                                                }
                                                ?>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"> <b>Username:</b></label>
                                            <div class="col-lg-7">
                                                <input type="text" name="username" class="form-control"
                                                       placeholder="Username must be 6-12 characters">
                                                <?php
                                                if (!empty($_SESSION['usr_err'])) {
                                                    echo $_SESSION['usr_err'];
                                                    unset($_SESSION['usr_err']);
                                                }
                                                if (!empty($_SESSION['usr_err2'])) {
                                                    echo $_SESSION['usr_err2'];
                                                    unset($_SESSION['usr_err2']);
                                                }
                                                if (!empty($_SESSION['usr_err3'])) {
                                                    echo $_SESSION['usr_err3'];
                                                    unset($_SESSION['usr_err3']);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"> <b>Email:</b></label>
                                            <div class="col-lg-7">
                                                <input type="text" name="email" class="form-control"
                                                       placeholder="Enter your email">
                                                <?php
                                                if (!empty($_SESSION['email_err'])) {
                                                    echo $_SESSION['email_err'];
                                                    unset($_SESSION['email_err']);
                                                }
                                                if (!empty($_SESSION['email_err2'])) {
                                                    echo $_SESSION['email_err2'];
                                                    unset($_SESSION['email_err2']);
                                                }
                                                if (!empty($_SESSION['email_err3'])) {
                                                    echo $_SESSION['email_err3'];
                                                    unset($_SESSION['email_err3']);
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"><b>Password:</b></label>
                                            <div class="col-lg-7">
                                                <input type="password" name="pass" class="form-control"
                                                       placeholder="Password must be 6-12 characters with an uppercase letter">
                                                <?php
                                                if (!empty($_SESSION['pass_err'])) {
                                                    echo $_SESSION['pass_err'];
                                                    unset($_SESSION['pass_err']);
                                                }
                                                if (!empty($_SESSION['pass_err2'])) {
                                                    echo $_SESSION['pass_err2'];
                                                    unset($_SESSION['pass_err2']);
                                                }
                                                if (!empty($_SESSION['pass_err3'])) {
                                                    echo $_SESSION['pass_err3'];
                                                    unset($_SESSION['pass_err3']);
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"><b>Repeat Password:</b></label>
                                            <div class="col-lg-7">
                                                <input type="password" name="repeat_pass" class="form-control"
                                                       placeholder="Repeat password">

                                                <?php
                                                if (!empty($_SESSION['pass_err'])) {
                                                    echo $_SESSION['pass_err'];
                                                    unset($_SESSION['pass_err']);
                                                }
                                                if (!empty($_SESSION['pass_err4'])) {
                                                    echo $_SESSION['pass_err4'];
                                                    unset($_SESSION['pass_err4']);
                                                }
                                                ?> </div>
                                        </div>
                                        <!--                                        <div class="form-group">-->
                                        <!--                                            <label class="control-label">Select User Photo</label>-->
                                        <!--                                            <div>-->
                                        <!--                                                <input type="file" name="image" class="form-control">-->
                                        <!--                                                <label class="validation-error-label"></label>-->
                                        <!--                                            </div>-->
                                        <!--                                        </div>-->
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"><b>Upload your picture:</b></label>
                                            <div class="col-lg-9">
                                                <input type="file" placeholder="upload your picture" name="image">
                                                <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>

                                        <div class="col-md-6">
                                            <div class="form-group has-feedback">
                                                <label style="margin-right: 20px;"><b>Select User Type : </b></label>
                                                <label><input type="radio" name="is_admin" value=""/> User </label>
                                                &nbsp;&nbsp;
                                                <label><input type="radio" name="is_admin" value="1" checked/> Admin
                                                </label>
                                                <label class="validation-error-label"></label>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="reset" class="btn btn-sm">Reset <i
                                                class="icon-reset position-left"></i></button>


                                        <button type="submit" class="btn bg-teal-400">Create Account<i
                                                class="icon-arrow-right6 position-right"></i></button>
                                    </div>
                                </div>


                            </form>
                            <!-- /basic layout -->


                            <!-- Main charts -->
                            <div class="row">
                                <div class="col-lg-5">


                                </div>
                            </div>
                            <!-- /main charts -->


                        </div>


                    </div>
                </div>
                <!-- /quick stats boxes -->


            </div>
        </div>
        <!-- /dashboard content -->
        <!-- Footer -->
        <!--        <div class="footer text-muted">-->
        <!--            &copy; 2016. <a href="#">BASIS Institute of Technology & Management</a> Website <a href="http://www.bitm.org.bd/" target="_blank">BITM.COM</a>-->
        <!--        </div>-->
        <!-- /footer -->
        <!--    </div>-->
        <!-- /content area -->

        <!-- Footer 1-->
        <!--<div class="footer text-muted">-->
        <!--&copy; 2016. <a href="#">BITM Lab Management</a> by <a href="#" target="_blank">Muhammad Redwan_SEIP-122515</a>-->
        <!--</div>-->
        <!-- /footer 1 -->


        <!-- Fixed Footer -->
        <div class="navbar navbar-default navbar-sm navbar-fixed-bottom">
            <ul class="nav navbar-nav no-border visible-xs-block">
                <li><a class="text-center collapsed" data-toggle="collapse"
                       data-target="#navbar-second"><i
                            class="icon-circle-up2"></i></a></li>
            </ul>

            <div class="navbar-collapse collapse" id="navbar-second">
                <div class="navbar-text">
                    &copy; 2016. <a href="#">BITM Lab Management</a> by <a href="#" target="_blank">Muhammad
                        Redwan_SEIP-122515</a>
                </div>

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Help center</a></li>
                        <li><a href="#">Policy</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <!-- /Fixed footer -->


    </div>
    <!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->


</body>

</html>

