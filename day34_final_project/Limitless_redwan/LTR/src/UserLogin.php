<?php

/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 9/7/2016
 * Time: 4:39 PM
 */

namespace ProjectApp;
class UserLogin
{

    public $name = '';
    public $username = '';
    public $email = '';
    public $password = '';
    public $repeat_pass = '';
    public $is_admin = '';

    public function __construct()
    {
        session_start();

        $conn = mysql_connect('localhost', 'root', '') or die('unable to connect with mysql');
        mysql_select_db('lab') or die('unable to connect with database');
    }

    public function prepare($data = '')
    {
//        $this->name = $data['full_name'];
//        $this->username = $data['username'];
//        $this->email = $data['email'];
//        $this->password = $data['pass'];
////        echo "this is a test";
//
//        return $this;


//        Full NAME
        if (empty($data['full_name'])) {
            $_SESSION['name_err'] = "<h6 style='color: red'>* Full Name is required</h6>";
        }
        elseif (strlen($data['full_name']) < 6  || strlen($data['full_name'] > 30)) {
            $_SESSION['name_err2'] = "<h6 style='color: red'>Full name must be 6 to 30 characters long</h6>";
        }

        else {
            $this->name = $data['full_name'];
        }


//        USER NAME
        if (empty($data['username'])) {
            $_SESSION['usr_err'] = "<h6 style='color: red'>* Username must be required</h6>";
        } elseif (strlen($data['username']) < 6) {
            $_SESSION['usr_err2'] = "<h6 style='color: red'>Input is too short, minimum is 6 characters (maximum 12).</h6>";
        } elseif (strlen($data['username']) > 12) {
            $_SESSION['usr_err3'] = "<h6 style='color: red'>Input is too LONG, Maximum 12 characters (min-6).</h6>";
        } else {
            $this->username = $data['username'];
        }
//EMAIL
        if (!empty($data["email"])) {
            $this->email = $data["email"];
            // check if e-mail address is well-formed
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $_SESSION['email_err2'] = "<h6 style='color: red'>* Invalid E-mail Format</h6>";
            }
        }
        else {
            $_SESSION['email_err'] = "<h6 style='color: red'>* E-mail must be required</h6>";
        }




        //PASSWORD
        if(!empty($data['pass']) && !empty($data['repeat_pass'])){

            $this->password = $data['pass'];
            $this->repeat_pass = $data['repeat_pass'];

            if (strlen($data['pass']) < 6) {
                $_SESSION['pass_err2'] = "<h6 style='color: red'>* Password is too SHORT, minimum is 6 characters (maximum 12).</h6>";
            }
            if ( strlen($data['pass']) > 12) {
                $_SESSION['pass_err3'] = "<h6 style='color: red'>* Password is too LONG, Maximum 12 characters (min-6).</h6>";
            }
            if ($this->password !== $this->repeat_pass)  {
                $_SESSION['pass_err4'] = "<h6 style='color: red'>Password must be same</h6>";
            }
        }

        else{

            $_SESSION['pass_err'] = "<h6 style='color: red'>* Password must be required</h6>";

        }


// IS ADMIN
        if (($data['is_admin'])==1){

            $this->is_admin = $data['is_admin'];
        }
        return $this;

    }

    public function store()
    {
        if (!empty($this->name) && !empty($this->username)&& !empty($this->email)&& !empty($this->email)&& !empty($this->password)&&  !empty($this->password == $this->repeat_pass)&& !empty($this->is_admin)) {
            $query = "INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`) VALUES (NULL, '" . uniqid() . "', '$this->name', '$this->username', '$this->email', '$this->password', '', '0', '$this->is_admin', '0', '', '', '')";

//        echo $query;
            if (mysql_query($query)) {
                $_SESSION ['submit_msg'] = '<h3 style="color:blue;">Data Successfully Submitted</h3>';
            }
            header('location:add_user.php');
        }
        else{
            header('location:add_user.php');
        }
    }
    
    
}