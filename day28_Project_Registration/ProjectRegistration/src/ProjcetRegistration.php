<?php


namespace Reg;

use PDO;

class ProjcetRegistration
{
    public $id = '';
    public $username = '';
    public $password = '';
    public $RepeatPassword = '';
    public $email = '';
    public $is_active = '';
    public $is_admin = '';
    public $created = '';
    public $modified = '';
    public $deleted = '';
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = '';


    public function __construct()
    {
        session_start();

        $this->conn = new PDO('mysql:host=localhost;dbname=redwan-php26', $this->dbuser, $this->dbpass);

    }

    public function prepare($data = '')
    {
        if (array_key_exists('id', $data)) {


        $this->id = $data['id'];
    }

//        USER NAME
        if (empty($data['username'])) {
            $_SESSION['usr_err3'] = "<h5 style='color: red'>* Username must be required</h5>";
        } elseif (strlen($data['username']) < 6) {
            $_SESSION['usr_err'] = "<h5 style='color: red'>Input is too short, minimum is 6 characters (maximum 12).</h5>";
        } elseif (strlen($data['username']) > 12) {
            $_SESSION['usr_err2'] = "<h5 style='color: red'>Input is too LONG, Maximum 12 characters (min-6).</h5>";
        } else {
            $this->username = $data['username'];
        }
        //EMAIL
        if (!empty($data['email'])) {
            $this->email = $data['email'];
        } else {
            $_SESSION['email_err'] = "<h5 style='color: red'>* E-mail must be required</h5>";
        }
        //PASSWORD
        if(!empty($data['password']) && !empty($data['repeat_pass'])){

        $this->password = $data['password'];
        $this->RepeatPassword = $data['repeat_pass'];
        }

        elseif(empty($data['password']) && empty($data['repeat_pass'])){

            $_SESSION['pass_err3'] = "<h5 style='color: red'>* Password must be required</h5>";

        }
        elseif (strlen($data['password']) < 6) {
            $_SESSION['pass_err'] = "<h5 style='color: red'>* Password is too SHORT, minimum is 6 characters (maximum 12).</h5>";
        } elseif ( strlen($data['password']) > 12) {
            $_SESSION['pass_err2'] = "<h5 style='color: red'>* Password is too LONG, Maximum 12 characters (min-6).</h5>";
        }
        if ($this->password !== $this->RepeatPassword)  {
                $_SESSION['pass_err4'] = "<h5 style='color: red'>Password must be same</h5>";
            }


            return $this;
        }

        public
        function store()
        {
            if (!empty($this->username) && !empty($this->email) && !empty($this->password) && $this->password == $this->RepeatPassword) {
                try {

                    $query = "INSERT INTO project_registration (id, unique_id, verification_id, username, password, repeat_password, email, is_active, is_admin, created, modified, deleted) VALUES(:i, :uid, :vid, :usr, :pass, :RepPass, :email, :isactv, :isadmn, :crt, :mdf, :dlt)";

                    $stmt = $this->conn->prepare($query);
                    $stmt->execute(array(
                        ':i' => null,
                        ':uid' => uniqid(),
                        ':vid' => uniqid(),
                        ':usr' => $this->username,
                        ':pass' => $this->password,
                        ':RepPass' => $this->RepeatPassword,
                        ':email' => $this->email,
                        ':isactv' => $this->is_active,
                        ':isadmn' => $this->is_admin,
                        ':crt' => date("Y-m-d H:i:s"),
                        ':mdf' => getdate(),
                        ':dlt' => getdate(),
                    ));

                    $_SESSION ['submit_msg'] = "<h2>Data successfully submitted</h2>";
                    header('location:create.php');
                } catch (PDOException $e) {
                    echo 'Error: ' . $e->getMessage();
                }
            } else {
                header('location:create.php');
            }
        }

        public
        function index()
        {

            $qr = "SELECT * FROM `project_registration`";
            $query = $this->conn->prepare($qr);
            $query->execute();

            while ($result = $query->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $result;
            }
//        echo "<pre>";
//        print_r($result);
//        print_r($this->data);
            return $this->data;

        }

        public
        function show()
        {
            $show_qr = "SELECT * FROM `project_registration` WHERE `unique_id`=" . "'".$this->id."'";
//        echo $show_qr;
            $sth = $this->conn->prepare($show_qr);
            $sth->execute();
            $result = $sth->fetch();

//        echo "<pre>";
//        print_r($result);

            return $result;
        }

        public
        function update()
        {
            $up_qry = "UPDATE `redwan-php26`.`project_registration` SET `password` ='$this->password' WHERE `project_registration`.`unique_id` =" ."'".$this->id."'";

//            echo $up_qry;
        $stmt = $this->conn->prepare($up_qry);
            $stmt->execute();
            header('location:index.php');
            
            $_SESSION['upd_msg']= "<h2 style='color: green'> Password Successfully Updated </h2>";
        }


    public function delete(){

    }

    }