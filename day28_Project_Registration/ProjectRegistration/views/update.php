<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/31/2016
 * Time: 1:36 AM
 */

include_once "../vendor/autoload.php";

use Reg\ProjcetRegistration;

$obj = new ProjcetRegistration();
$obj->prepare($_POST)->update();