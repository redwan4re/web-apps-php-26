<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/30/2016
 * Time: 3:24 PM
 */

include "../vendor/autoload.php";
use Reg\ProjcetRegistration;

$obj = new ProjcetRegistration();

$edit = $obj->prepare($_GET)->show();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bootsnip.css">

    <!-- Website CSS style -->
    <link rel="stylesheet" type="text/css" href="bootsnip.css">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

    <title>Update/Change Password</title>
</head>
<body>
<a href="../index.php"> Back to Project Homepage</a> |
<a href="index.php"> Back to User List</a>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 style="text-align: center" class="title">Update Password</h1>
                <hr/>
            </div>
        </div>
        <div class="main-login main-center">
            <form class="form-horizontal" method="post" action="update.php">
                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Old Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="password" id="password"
                                   value="<?php echo $edit['password'];?>"> <?php
                            if (isset($_SESSION['pass_err']) && !empty($_SESSION['pass_err'])) {
                                echo $_SESSION['pass_err'];
                                unset($_SESSION['pass_err']);
                            }

                            if (isset($_SESSION['pass_err2']) && !empty($_SESSION['pass_err2'])) {
                                echo $_SESSION['pass_err2'];
                                unset($_SESSION['pass_err2']);
                            }

                            if (isset($_SESSION['pass_err3']) && !empty($_SESSION['pass_err3'])) {
                                echo $_SESSION['pass_err3'];
                                unset($_SESSION['pass_err3']);
                            } ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">New Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password"
                                   placeholder="Password (6-12 characters)"/> <?php
                            if (isset($_SESSION['pass_err']) && !empty($_SESSION['pass_err'])) {
                                echo $_SESSION['pass_err'];
                                unset($_SESSION['pass_err']);
                            }

                            if (isset($_SESSION['pass_err2']) && !empty($_SESSION['pass_err2'])) {
                                echo $_SESSION['pass_err2'];
                                unset($_SESSION['pass_err2']);
                            }

                            if (isset($_SESSION['pass_err3']) && !empty($_SESSION['pass_err3'])) {
                                echo $_SESSION['pass_err3'];
                                unset($_SESSION['pass_err3']);
                            } ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Repeat Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="repeat_pass" id="confirm"
                                   placeholder="Confirm your Password "/><?php
                            if (isset($_SESSION['pass_err3']) && !empty($_SESSION['pass_err3'])) {
                                echo $_SESSION['pass_err3'];
                                unset($_SESSION['pass_err3']);
                            }

                            if (isset($_SESSION['pass_err4']) && !empty($_SESSION['pass_err4'])) {
                                echo $_SESSION['pass_err4'];
                                unset($_SESSION['pass_err4']);
                            } ?>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                    </div>
                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                </div>
                <!--						<div class="login-register">-->
                <!--				            <a href="index.php">Login</a>-->
                <!--				         </div>-->
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</body>

<footer id="footer">
    Created By-Muhammad Redwan | SEID: 122515 | WebApps Development-PHP(26th Batch), BITM | Mentor: Md. Abu Taleb
    (Sumon)
</footer>

</html>
