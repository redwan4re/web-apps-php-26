<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/29/2016
 * Time: 12:15 PM
 */
include_once "../vendor/autoload.php";
use Reg\ProjcetRegistration;

$obj = new ProjcetRegistration();
$Alldata = $obj->index();
//echo $Alldata;
//echo "<pre>";
//print_r($Alldata);

?>
<?php

if (isset($_SESSION['upd_msg']) && !empty($_SESSION['upd_msg'])) {
    echo $_SESSION['upd_msg'];
    unset($_SESSION['upd_msg']);
}
if (isset($_SESSION['dlt_msg']) && !empty($_SESSION['dlt_msg'])) {
    echo $_SESSION['dlt_msg'];
    unset($_SESSION['dlt_msg']);
}

?>
    <html>
    <title>Index | Data</title>
<body style="align-content: center">
    <h3 align="center"><a href="../index.php"> Back to Project Homepage</a>
    </h3>

    <h4 align="center"><a href=create.php>User Registration Form</a>
    </h4>

    <form action="search_results.php" method="post" >

        <input type="text" name="search" placeholder="Search By Name/ semester/ cost etc" size="30" maxlength="50">
        <input type="submit" value="Search">

    </form>

<table border="3" align="center">
    <tr>
        <th>SL</th>
        <th>Username</th>
        <th>Email</th>
        <th>Password</th>
        <th>Is Active</th>
        <th>Is Admin</th>
        <th colspan="3">Action</th>
    </tr>

<?php
$serial = 1;
if (isset($Alldata) && !empty($Alldata)) {

    foreach ($Alldata as $Singledata) {
//                echo "<pre>";
//                print_r($Singledata);
        ?>
        <tr>
            <td><?php echo $serial++; ?></td>
            <td><?php echo $Singledata['username']; ?></td>
            <td><?php echo $Singledata['email']; ?></td>
            <td><?php echo $Singledata['password'] ?></td>
            <td><?php echo $Singledata['is_active']; ?></td>
            <td><?php echo $Singledata['is_admin']; ?></td>
            <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>
        </tr>

    <?php }
} else {


    ?>

    <tr>
        <td colspan="7">
            No available Data
        </td>
    </tr>
    </table>
    </body>
    </html>
    <?php
}
?>