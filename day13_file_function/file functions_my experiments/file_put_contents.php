<?php


//  syntax: file_put_contents(file,data,mode,context) 

$file = "test.txt";
$string = "Muhammad Redwan Dewan_BITM_122515";


//file_put_contents("$file", "$string");

file_put_contents("test.doc", "Muhammad Redwan 01");

file_put_contents("test.xls", "Muhammad Redwan 02");

//  This is possible for basic documents like- .txt, .doc, .xls BUT not supported .docx, .xlsx, .pdf etc.
// file_put_contents("test.xlsx", "Muhammad Redwan 03");
// file_put_contents("test.docx", "Muhammad Redwan 04");


if (file_put_contents("$file", "$string")) {
    echo "successfully written in document";
}
