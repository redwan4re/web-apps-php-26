<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/21/2016
 * Time: 1:05 AM
 */

include_once "../../../../vendor/autoload.php";
use App\Bitm\seid_122515\SemesterEnrollment\SemesterEnrollment;

$obj = new SemesterEnrollment();

$onedata = $obj->prepare($_GET)->show();

//is equivalent to obj->prepare($_GET);$obj->show();

//echo "<pre>";
//print_r($onedata);

if(isset($onedata) && !empty($onedata)){


?>

<html>
<body>
<h3 align="center"><a href="index.php">Back to list</a></h3>
<table border="3" align="center">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Unique ID</th>
    </tr>
    <tr>
        <td><?php echo $onedata['id'];?></td>
        <td><?php echo $onedata['name'];?></td>
        <td><?php echo $onedata['unique_id'];?></td>
    </tr>
</table>
</body>
</html>

<?php } else{
    $_SESSION['err_msg']="Not found. Something went wrong!!";

    header('location:errors.php');
}


?>


