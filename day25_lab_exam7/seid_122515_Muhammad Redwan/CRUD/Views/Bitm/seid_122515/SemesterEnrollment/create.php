<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/19/2016
 * Time: 4:16 PM
 */
session_start();

?>

    <html>
    <head>
        <title>SemesterEnrollment</title>
    </head>
    <body style="align-content: center">
    <h3>
        <a href="../../../../../index.php"> Back to Project Homepage</a></h3>
    <h4><a href="index.php"> Back to Student Enrollment</a>
    </h4>

    <form action="store.php" method="post">
        <fieldset>
            <legend><b>Semester Enrollment</b></legend> <br>
            <label>Your Name</label>
            <input type="text" name="name" placeholder="Enter Your Name">
            <?php
            if(!empty($_SESSION['name_err'])){
                echo $_SESSION['name_err'];
                unset($_SESSION['name_err']);
            }
            ?>
            <br/> <br/>
            <label>Semester (1 to 3)</label>
            <input type="text" name="semester">
            <?php
            if(!empty($_SESSION['sem_err'])){
                echo $_SESSION['sem_err'];
                unset($_SESSION['sem_err']);
            }
            ?>
            <br/> <br/>
            <label>Get Offer</label>
            <input type="checkbox" name="get_offer" value="Yes"
            > <br><br/>
            <input type="submit" value="Submit">
        </fieldset>
    </form>
    </>
    </html>

<?php
if (isset($_SESSION['sbmt_msg']) && !empty($_SESSION['sbmt_msg'])) {
    echo $_SESSION['sbmt_msg'];
    unset($_SESSION['sbmt_msg']);
}
?>