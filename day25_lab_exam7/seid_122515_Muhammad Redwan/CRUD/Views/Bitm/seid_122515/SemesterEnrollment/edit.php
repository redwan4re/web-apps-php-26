<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/21/2016
 * Time: 3:15 AM
 */

include_once "../../../../vendor/autoload.php";
use App\Bitm\seid_122515\SemesterEnrollment\SemesterEnrollment;

$obj = new SemesterEnrollment();

$obj->prepare($_GET);

$edit = $obj->show();

//echo "<pre>";
//print_r($edit);

//echo "<pre>";
//print_r($_GET);


?>
<html>
<head>
    <title>Update SemesterEnrollment</title>
</head>
<body style="align-content: center">
<h3>
    <a href="../../../../../index.php"> Back to Project Homepage</a></h3>
<h4><a href="index.php"> Back to Student Enrollment</a>
</h4>

<form action="update.php" method="POST">
    <fieldset>
        <legend><b>Update Semester Enrollment</b></legend> <br>
        <label>Your Name</label>
        <input type="text" name="name" value="<?php echo $edit['name'];?>">
<!--        --><?php
//        if(!empty($_SESSION['name_err'])){
//            echo $_SESSION['name_err'];
//            unset($_SESSION['name_err']);
//        }
//        ?>
        <br/> <br/>
        <label>Semester (1 to 3)</label>
        <input type="text" name="semester" value="<?php echo $edit['semester'];?>">
<!--        --><?php
//        if(!empty($_SESSION['sem_err'])){
//            echo $_SESSION['sem_err'];
//            unset($_SESSION['sem_err']);
//        }
//        ?>
        <br/> <br/>
        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
        <label>Get Offer</label>
        <input type="checkbox" name="get_offer" value="Yes"> <br><br/>
        <input type="submit" value="Update">
    </fieldset>
</form>
</>
</html>

<?php
if (isset($_SESSION['upd_msg']) && !empty($_SESSION['upd_msg'])) {
    echo $_SESSION['upd_msg'];
    unset($_SESSION['upd_msg']);
}


?>

