<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/26/2016
 * Time: 2:17 AM
 */

session_start();

if (isset($_SESSION['err_msg']) && !empty($_SESSION['err_msg'])){
    echo "<h1>".$_SESSION['err_msg']."</h1>";
    unset($_SESSION['err_msg']);
}