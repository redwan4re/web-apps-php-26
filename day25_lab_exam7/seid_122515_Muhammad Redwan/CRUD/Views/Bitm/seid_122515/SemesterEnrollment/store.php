<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/19/2016
 * Time: 4:19 PM
 */

include_once "../../../../vendor/autoload.php";
use App\Bitm\seid_122515\SemesterEnrollment\SemesterEnrollment;

//echo "<pre>";
//print_r($_POST);

$obj = new SemesterEnrollment();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $obj->prepare($_POST);
    $obj->store();

}

else{
    $_SESSION['err_msg']= "OOps!! You are not authorised to this page";
    header('location:errors.php');
}



