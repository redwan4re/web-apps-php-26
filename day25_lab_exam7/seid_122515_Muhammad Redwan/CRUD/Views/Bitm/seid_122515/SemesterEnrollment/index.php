<?php
include_once "../../../../vendor/autoload.php";
use App\Bitm\seid_122515\SemesterEnrollment\SemesterEnrollment;

$obj = new SemesterEnrollment();
$Alldata = $obj->index();
//echo $Alldata;
//echo "<pre>";
//print_r($Alldata);

?>
<?php

if (isset($_SESSION['dlt_msg']) && !empty($_SESSION['dlt_msg'])) {
    echo $_SESSION['dlt_msg'];
    unset($_SESSION['dlt_msg']);
}

?>
    <html>
    <title>Index | Data</title>
<body style="align-content: center">
    <h3 align="center"><a href="../../../../../index.php"> Back to Project Homepage</a>
    </h3>

    <h4 align="center"><a href=create.php>Add Student info</a>
    </h4>

    
<table border="3" align="center">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Semester</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver Amount</th>
        <th>Total</th>
        <th colspan="3">Action</th>
    </tr>

<?php
$serial = 1;
if (isset($Alldata) && !empty($Alldata)) {

    foreach ($Alldata as $Singledata) {
//        echo "<pre>";
//        print_r($Singledata);
        ?>
        <tr>
            <td><?php echo $serial++; ?></td>
            <td><?php echo $Singledata['name']; ?></td>
            <td><?php echo $Singledata['semester']; ?></td>
            <td><?php if (!empty($Singledata['offer'])) {
                    echo "<input type='checkbox' name='Yes' value='Yes' checked>";
                    //echo "Yes";
                } else {
                    echo "<input type='checkbox' name='No' value='NO' >";
                    //echo "No";
                } ?></td>
            <td><?php echo $Singledata['cost']; ?></td>
            <td><?php echo $Singledata['waiver']; ?></td>
            <td><?php echo $Singledata['total']; ?></td>
            <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
            <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>
        </tr>

    <?php }
} else {


    ?>

    <tr>
        <td colspan="8">
            No available Data
        </td>
    </tr>
    </table>
    </body>
    </html>
    <?php
}
?>