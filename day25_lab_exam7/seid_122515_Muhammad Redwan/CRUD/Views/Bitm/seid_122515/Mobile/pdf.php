<?php
include ('../../../../vendor/mpdf/mpdf/mdf.php');
include_once "../../../../vendor/autoload.php";

use App\Bitm\seid_122515\Mobile\Mobile;

$obj = new Mobile();
$allData = $obj->index();

$trs="";
$serial=0;
foreach($allData as $data):
$serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$data['id']."</td>";
$trs.="<td>".$data['title']."</td>";
$trs.="<td>".$data['unique_id']."</td>";
$trs.="</tr>";
endforeach;

$html= <<<EOD
<!DOCTYPE html>
<html>
<head>
	<title>Mobile Model</title>
</head>
<body>
<h1>List of mobile</h1>
<table border="1">
<thead>
  <tr>
     <th>Sl</th>
     <th>id</th>
     <th>title</th>
     <th>Unique ID</th>
     </tr>
     </thead>

 <tbody>
 <tr>
 $trs;
 </tr>
 </tbody>
 </table>
</body>
</html>

EOD;


$mpdf = new mPDF();

$mpdf->WriteHTML($html);

$mpdf->Output();

?>
