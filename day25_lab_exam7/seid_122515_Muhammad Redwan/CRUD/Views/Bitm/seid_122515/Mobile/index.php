<?php
//include_once "../../../../Src/Bitm/seid_122515/Mobile/Mobile.php";
include_once "../../../../vendor/autoload.php";
?>
<h3><a href="../../../../../index.php">Back to Project HomePage</a></h3>

    <h4><a href="create.php">Add New Mobile Model</a></h4>  <h5><a href="pdf.php">Download As PDF | <a href="xl.php">Download As Excel</a></a></h5>

    <?php
    use App\Bitm\seid_122515\Mobile\Mobile;

    $obj = new Mobile();
    $Alldata = $obj->index();

    if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
    ?>
    <html>
    <head>
        <title>Index | Data</title>
    </head>
    <body bgcolor="#f0f8ff" align="center">
    <table border="2" align="center" width="500">
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th colspan="3">Action</th>
        </tr>
        <?php
        $serial = 1;
        if (isset($Alldata) && !empty($Alldata)) {

            foreach ($Alldata as $Singledata) {
                ?>

                <tr>
                    <td><?php echo $serial++ ?></td>
                    <td><?php echo $Singledata['title'] ?></td>
                    <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
                    <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
                    <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>

                </tr>
            <?php }
        } else {
            ?>
            <tr>
                <td colspan="3">
                    No available data
                </td>
            </tr>
        <?php } ?>
    </table>
    </body>
    </html>
