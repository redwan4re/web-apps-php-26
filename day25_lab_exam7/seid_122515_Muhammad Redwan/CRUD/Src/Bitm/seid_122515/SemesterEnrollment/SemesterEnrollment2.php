<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 7/19/2016
 * Time: 4:14 PM
 */

namespace App\Bitm\seid_122515\SemesterEnrollment2;


class SemesterEnrollment2
{
    public $id = '';
    public $name = '';
    public $semester = '';
    public $offer = '';
    public $data = '';
    public $cost = '';
    public $waiver = '';
    public $total = '';


    public function __construct()
    {
        session_start();
        $conn = mysql_connect('localhost', 'root', '') or die("Unable to connect with MYSQL");

        mysql_select_db('redwan-php26') or die('Unable to connect with Database');
    }

    public function prepare($data = '')
    {
//        print_r($data);
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('semester', $data)) {
            $this->semester = $data['semester'];
        }

        if (array_key_exists('get_offer', $data)) {
            $this->offer = $data['get_offer'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if ($this->semester == 1 && !empty($this->offer)) {
            $this->cost = '9000';
            $this->waiver = '900';
            $this->total = $this->cost - $this->waiver;
        } else {
            $this->cost = '9000';
            $this->waiver = '0';
            $this->total = '9000';
        }
        if ($this->semester == 2 && !empty($this->offer)) {
            $this->cost = '10000';
            $this->waiver = $this->cost * 15 / 100;
            $this->total = $this->cost - $this->waiver;
        } else {
            $this->cost = '10000';
            $this->waiver = '0';
            $this->total = '10000';
        }
        if ($this->semester == 3 && !empty($this->offer)) {
            $this->cost = '11000';
            $this->waiver = $this->cost * 20 / 100;
            $this->total = $this->cost - $this->waiver;
        }

        else {
            $this->cost = '11000';
            $this->waiver = '0';
            $this->total = '11000';
        }

        return $this;

//     print_r($this->name);
//   print_r($this->offer);
//    echo $this->name;
// echo $this->offer;
//        echo "<pre>";
//        print_r($this);
    }

    public function store()
    {
        $query = "INSERT INTO `redwan-php26`.`semester_enrollment` (`id`, `name`,`semester`,`offer`,`cost`, `waiver`,`total`) VALUES (NULL, '$this->name','$this->semester','$this->offer','$this->cost', '$this->waiver', '$this->total')";
        echo $query;
        if (mysql_query($query)) {
            $_SESSION['msg'] = "<h3 style=color:deepskyblue>" . "Data successfully submitted" . "</h3>";
        }
        header('location:create.php');
    }

    public function index()
    {
        $qr = "SELECT * FROM `semester_enrollment`";
        $mydata = mysql_query($qr);
        while ($row = mysql_fetch_assoc($mydata)) {
            $this->data[] = $row;
        };
        return $this->data;

    }

    public function show()
    {
//       echo $this->id;
        $qr2 = "SELECT * FROM `semester_enrollment` WHERE id=" . $this->id;
//        echo $qr2;
        $mydata2 = mysql_query($qr2);
        $row = mysql_fetch_assoc($mydata2);

        return $row;
    }


    public function delete()
    {
        $dlt_qr = "DELETE FROM `semester_enrollment` WHERE `semester_enrollment`.`id` =" . $this->id;

//        echo "i would like to delete". $this->id;
//        echo $dlt_qr;

        if (mysql_query($dlt_qr)) {
            $_SESSION['msg'] = "<h3 style=color:red; align='center'>" . "Data successfully DELETED" . "</h3>";
        }
        header('location:index.php');
    }
}
