<?php
//this is for constant variable
define('bitm', 'I am a student of BITM');

echo bitm;
echo "<br/>";

// this is used for showing the line number 
echo __line__ ;
echo "<br/>";

// this is used for showing the full file name
echo  __FILE__;
echo "<br/>";

// this is used for showing the directory path
echo __DIR__;
echo "<br/>";

function myname () {
    echo "My name is Muhammad Redwan <br>";
    echo __FUNCTION__;
}
myname();

echo "<br/>";

echo __TRAIT__;
echo "<br/>";

echo __line__ ;

//echo phpinfo();

echo "<br/>";

echo !0;
