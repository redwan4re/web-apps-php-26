<?php

//THIS IS STRING FUNCTION: ADDSLASH

//$str = "I like Bangladesh";
//
//// Outputs: Is your name O\'Reilly?
//
//echo addslashes($str);


$str = addslashes('What does "yolo" mean?');
echo($str);

// THIS IS STRING FUNCTION:EXPLODE>> makes a string to an array

//ECHO "<PRE>";
////$str = "Hello world. It's a beautiful day.";
//$str= "hello world, this is Muhammad Redwan.";
//print_r (explode(".",$str));
//ECHO "</PRE>";

ECHO "<PRE>";
//$str = "Hello world. It's a beautiful day.";
$str= "hello world, this is Muhammad Redwan.";
print_r (explode(" ",$str));
ECHO "</PRE>";

echo "<br>";
//  THIS IS STRING FUNCTION: implode >> makes an array to an string
$arr = array('Hello','World!','Beautiful','Day!');
echo implode("",$arr);
echo "<br>";

//  THIS IS STRING FUNCTION: HTMLENTITIES
$str = "<© W3Sçh°°¦§>";
echo htmlentities($str);
echo "<br>";

//The str_pad() function pads a string to a new length.
//Pad to the right side of the string, to a new length of 20 characters: 

echo "<br>";

$str = "Hello str_pad";
echo str_pad($str,20,".");

//The str_replace() function replaces some characters with some other characters in a string.
//Replace the characters "world" in the string "Hello world!" with "Peter":        

echo str_replace("world","Peter","Hello world!");

//The str_split() function splits a string into an array.
ECHO "<PRE>";
print_r(str_split("Hello!", "3"));
ECHO "</PRE>";
echo "<br>";

//The strtolower() function Convert all characters to lowercase: 
 echo strtolower("Hello WORLD.");
echo "<br>";

 //The strtoupper() function Convert all characters to lowercase: 
 echo strtoupper("Hello WORLD.");
echo "<br>";

//substr_count() function Count the number of times "world" occurs in the string:
//syntax: substr_count(string,substring,start,length) 
 echo substr_count("Hello world. The world is nice","o");
 
 //PHP ucfirst() Function Convert the first character of "hello" to uppercase:
 
 echo ucfirst("hello world!");
 
 