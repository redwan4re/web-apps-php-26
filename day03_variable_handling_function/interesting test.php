<?php

// interesting test

//
//$a= "bangladesh";
//$b= "$a";
//$c= 'b';

//echo $c;

//the output will be "b" not "bangladesh"---since b is between single quotes

$a= "I love Bangladesh";
$b= $a;
$c= $b;

echo $c;

//the output will be "I love Bangladesh"

//"$a" means the same as $a; so, the output will be the same as above.

//$a= "I love Bangladesh";
//$b= "$a";
//$c= "$b";
//
//echo $c;