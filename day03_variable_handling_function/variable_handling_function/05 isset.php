<?php

// The isset () function is used to check whether a variable is set or not. 
// If a variable is already unset with unset() function, it will no longer be set. 
// The isset() function return FALSE if testing variable contains a NULL value.
    
/*Example-1
 */
$data= null;

if (isset($data))
{
    echo "good, variable is set";
}

else
{
    echo "Variable not set yet";
}
echo "<br>";

//example-2
$arr2= array('First Name'=>"Muhammad", 'Last Name'=>"Redwan", 'google', 'mahmud');

if (isset($arr2[2])){
    echo "Good";
//    here, index 0=google, index 1= mahmud and index 2,3 and more doesn't exist.
}
else{
    echo "Bad";
}

 echo "<br>";  

//example-3

$name=Redwan;
//$name=null;

if(isset($name)){
   echo"My name is $name"; 
}
else{
    echo"No name";
}

