<?php

/*var_dump  function will display/ show structured information
 about one or more expressions that includes its type and value.
 */

//example-1:

$a= 17.017;
$b= TRUE;

var_dump($a, $b);

echo "<br>";
//example-2:

$arr= array('My','Name', 'is', 'redwan');

var_dump($arr);


