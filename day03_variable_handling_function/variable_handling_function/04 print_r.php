<?php

// this assignment is about variable handling function PRINT_R 

$arr= array('My', 'name', 'is', 'redwan');
// its an index array

$arr2= array('First Name'=>"Muhammad", 'Last Name'=>"Redwan", 'google', 'mahmud');
//its an associate array(first 2 part)+index array (google, mahmud)

//echo "$arr";
//this will generate an error: Array to string conversion


echo "<pre>";
print_r($arr);

echo "<br>";

print_r( $arr2);

echo "</pre>";

// array can't  ECHO directly without index number of the array. such as--

//echo $arr[3];
//
//echo "<br>";
//
//echo $arr2['First Name'];