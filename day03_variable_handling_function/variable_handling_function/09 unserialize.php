<?php

// The unserialize() function converts to actual data from serialized data.

$serialized_data = serialize(array('My', 'Name', 'is', 'Redwan'));
echo  $serialized_data . "<br>";

// Unserialize the data  
$var1 = unserialize($serialized_data);

// Show the unserialized data;  
//var_dump ($var1);

print_r($var1);