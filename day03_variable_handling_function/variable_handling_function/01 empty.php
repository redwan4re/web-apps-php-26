<?php

// The empty() function is used to check whether a variable is empty or not.

$a=0;
$myName="Muhammad Redwan";

if (empty($a))
{
    echo "yes, This is EMPTY or 0 <br>";
}
else
{
    echo "there are some values <br>";
}

if (empty($myName))
{
    echo "yes, This is EMPTY or 0";
}
else
{
    echo "there are some values";
}

        
?>
