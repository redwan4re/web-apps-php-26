<?php

// this assignment is about variable handling function IS_INT 
        
// The is_int () function is used to test whether the type of the specified variable is an integer or not.

// TRUE if var_name is an integer, FALSE otherwise. Value Type : Boolean.

$var_name1=678;
$var_name2="678";
$var_name3=678.17;

if (is_int($var_name1))
{
    echo "$var_name1 is an integer <br>";
}
 else {
    echo "$var_name1 is not an integer <br>";    
}


if (is_int($var_name2))
{
    echo "$var_name2 is an integer <br>";
}
 else {
    echo "$var_name2 is not an integer <br>";    
}

if (is_int($var_name3))
{
    echo "$var_name3 is an integer <br>";
}
 else {
    echo "$var_name3 is not an integer <br>";    
}

?>
