<?php

// The unset() function destroys a given variable.

$name = "Muhammad Redwan";
//echo $name;
unset($name);
echo "<br>";
//$name= 17;
echo $name;

echo "<br>";
//output>> undefined error message.

//if variable has different values; the last value will be echo.


$a="old";
$a="new";
$a="latest";

echo $a;
// output>> latest

