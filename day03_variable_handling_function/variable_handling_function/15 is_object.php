<?php

// this assignment is about variable handling function IS_OBJECT
// The is_object () function is used to find whether a variable is object or not.


function get_subjects($obj_name)  
{  
if(!is_object($obj_name))  
{  
return(false);  
}  
return($obj_name->subjects); 
}
$obj_name = new stdClass;  
$obj_name->subjects = Array('Physics', 'Chemistry', 'Mathematics');  
var_dump(get_subjects(NULL));  
var_dump(get_subjects($obj_name)); 
?>
