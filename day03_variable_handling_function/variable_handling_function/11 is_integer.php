<?php

// this assignment is about variable handling function IS_INTEGER. 
        
// The is_integer() function is used to test whether the type of the specified variable is an integer or not.
// This function is an alias of: is_int()
// TRUE if var_name is an integer, FALSE otherwise. Value Type : Boolean.

$var_name1=678;
$var_name2="678";
$var_name3=678.17;

if (is_integer($var_name1))
{
    echo "$var_name1 is an integer <br>";
}
 else {
    echo "$var_name1 is not an integer <br>";    
}


if (is_integer($var_name2))
{
    echo "$var_name2 is an integer <br>";
}
 else {
    echo "$var_name2 is not an integer <br>";    
}

if (is_integer($var_name3))
{
    echo "$var_name3 is an integer <br>";
}
 else {
    echo "$var_name3 is not an integer <br>";    
}

?>
