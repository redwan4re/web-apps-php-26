<?php

// The serialize() function converts a storable representation of a value.

/*A serialize data means sequence of bits so that it can be stored in a file,
 a memory buffer, or transmitted across a network connection link*/


$serialized_data = serialize(array('My', 'Name', 'is', 'Redwan'));
echo  $serialized_data . '<br>';
