<?php

// The is_null () function is used to test whether a variable is NULL or not.

//Returns TRUE if var_name is null, FALSE otherwise. Value Type : Boolean.

$var_name = "redwan";

if (is_null($var_name)) {
    echo "variable is NULL";
} else {
    echo "variable is not NULL";
}

?>
