<?php

// this assignment is about variable handling function BOOLVAL. 

// PHP boolval function is used to get a boolean value from the given input value. 
// It returns "true" on success and "false" on failure.


echo (boolval(0) ? 'true' : 'false')."<br />";
echo (boolval(0.0) ? 'true' : 'false')."<br />";

echo (boolval(17) ? 'true' : 'false')."<br />";
echo (boolval(4.2) ? 'true' : 'false')."<br />";
        
?>
