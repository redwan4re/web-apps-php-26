<?php
/**
 * Created by PhpStorm.
 * User: Redwan
 * Date: 8/7/2016
 * Time: 1:26 AM
 */ ?>

<HTML>
<HEAD>
    <TITLE>Update Contact Information Checklist</TITLE>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">

</HEAD>
<BODY>


<h1>Edit Profile</h1>
<hr>

<h3>Personal info</h3>

<form class="form-horizontal" role="form" method="post">
    <div class="form-group">
        <label class="col-lg-3 control-label">First name:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" value="Muhammad">
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Last name:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" value="Redwan">
        </div>
    </div>


    <div class="form-group">
        <label class="col-lg-3 control-label">Gender:</label>
        <div class="col-lg-8">
            <select name="gender">
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">Race / Ethnicity:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">Nationality:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" value="Bangladesh">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">National ID:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-3 control-label">Father's Name:</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" value="Dewan Abdul Quadir">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Date of Birth:</label>
        <div class="col-md-8">
            <input class="form-control" type="date">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Blood Group:</label>
        <div class="col-md-8">
            <select class="form-group" type="text" >
                <option value=" " > Select Blood Group </option>
                <option value="1" > A+ </option>
                <option value="2" > A- </option>
                <option value="3" > B+ </option>
                <option value="4" > B- </option>
                <option value="5" > AB+ </option>
                <option value="6" > AB- </option>
                <option value="7" > O+ </option>
                <option value="8" > O- </option>
            </select>

        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Mobile:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Address1:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Address2:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">City:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Zip/Postal Code:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Interest / Hobby:</label>
        <div class="col-md-8">
            <input class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">Bio:</label>
        <div class="col-md-8">
                            <textarea name="message" rows="10" cols="30">
Type about yourself
</textarea>
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-8">
            <input type="button" class="btn btn-primary" value="Save Changes">
            <span></span>
            <input type="reset" class="btn btn-default" value="Cancel">
        </div>
    </div>
</form>
</div>
</div>
</div>
<hr>

</form>

</BODY>
</HTML>
