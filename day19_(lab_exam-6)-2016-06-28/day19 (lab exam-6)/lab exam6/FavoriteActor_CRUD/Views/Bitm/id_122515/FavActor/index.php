<?php
include_once "../../../../Src/Bitm/id_122515/FavActor/FavActors.php";
?>
<a href="../../../../index.php">List of Favorite Actors</a>
<br/>
<a href="create.php">Add Favorite Actors Name</a>
<br/>
<?php
$obj = new FavActors();
$Alldata = $obj->index();

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['title'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
