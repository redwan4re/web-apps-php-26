<?php
session_start();
?>
<a href="create.php">Add new</a>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Mobile Model</th>
        <th>Laptop Model</th>
        <th colspan="3">Action</th>
    </tr>
    <?php
    $serial = 1;
    if (isset($_SESSION['All_Data']) && !empty($_SESSION['All_Data'])) {

        foreach ($_SESSION['All_Data'] as $key=> $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php  if(isset($Singledata['mobile_model'])){echo$Singledata['mobile_model']; } ?></td>
                <td><?php  if(isset($Singledata['laptop_model'])){echo$Singledata['laptop_model']; } ?></td>
                <td><a href="show2.php?id=<?php echo $key ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $key ?>">Edit</a></td>
                <td><a href="delete2.php?id=<?php echo $key ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>
